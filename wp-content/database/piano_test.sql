/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100203
 Source Host           : localhost:3306
 Source Schema         : piano_test

 Target Server Type    : MySQL
 Target Server Version : 100203
 File Encoding         : 65001

 Date: 24/05/2018 15:42:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for piano_commentmeta
-- ----------------------------
DROP TABLE IF EXISTS `piano_commentmeta`;
CREATE TABLE `piano_commentmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `comment_id`(`comment_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_comments
-- ----------------------------
DROP TABLE IF EXISTS `piano_comments`;
CREATE TABLE `piano_comments`  (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime(0) NOT NULL,
  `comment_date_gmt` datetime(0) NOT NULL,
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`comment_ID`) USING BTREE,
  INDEX `comment_post_ID`(`comment_post_ID`) USING BTREE,
  INDEX `comment_approved_date_gmt`(`comment_approved`, `comment_date_gmt`) USING BTREE,
  INDEX `comment_date_gmt`(`comment_date_gmt`) USING BTREE,
  INDEX `comment_parent`(`comment_parent`) USING BTREE,
  INDEX `comment_author_email`(`comment_author_email`(10)) USING BTREE,
  INDEX `woo_idx_comment_type`(`comment_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_comments
-- ----------------------------
INSERT INTO `piano_comments` VALUES (1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-05-16 03:29:56', '2018-05-16 03:29:56', 'Hi, this is a comment.\r\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\r\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- ----------------------------
-- Table structure for piano_links
-- ----------------------------
DROP TABLE IF EXISTS `piano_links`;
CREATE TABLE `piano_links`  (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime(0) NOT NULL,
  `link_rel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`) USING BTREE,
  INDEX `link_visible`(`link_visible`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_options
-- ----------------------------
DROP TABLE IF EXISTS `piano_options`;
CREATE TABLE `piano_options`  (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`) USING BTREE,
  UNIQUE INDEX `option_name`(`option_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 642 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_options
-- ----------------------------
INSERT INTO `piano_options` VALUES (1, 'siteurl', 'http://localhost/wordpressWoo', 'yes');
INSERT INTO `piano_options` VALUES (2, 'home', 'http://localhost/wordpressWoo', 'yes');
INSERT INTO `piano_options` VALUES (3, 'blogname', 'Piano', 'yes');
INSERT INTO `piano_options` VALUES (4, 'blogdescription', 'Just another WordPress site', 'yes');
INSERT INTO `piano_options` VALUES (5, 'users_can_register', '0', 'yes');
INSERT INTO `piano_options` VALUES (6, 'admin_email', 'hieuhaono@gmail.com', 'yes');
INSERT INTO `piano_options` VALUES (7, 'start_of_week', '1', 'yes');
INSERT INTO `piano_options` VALUES (8, 'use_balanceTags', '0', 'yes');
INSERT INTO `piano_options` VALUES (9, 'use_smilies', '1', 'yes');
INSERT INTO `piano_options` VALUES (10, 'require_name_email', '1', 'yes');
INSERT INTO `piano_options` VALUES (11, 'comments_notify', '1', 'yes');
INSERT INTO `piano_options` VALUES (12, 'posts_per_rss', '10', 'yes');
INSERT INTO `piano_options` VALUES (13, 'rss_use_excerpt', '0', 'yes');
INSERT INTO `piano_options` VALUES (14, 'mailserver_url', 'mail.example.com', 'yes');
INSERT INTO `piano_options` VALUES (15, 'mailserver_login', 'login@example.com', 'yes');
INSERT INTO `piano_options` VALUES (16, 'mailserver_pass', 'password', 'yes');
INSERT INTO `piano_options` VALUES (17, 'mailserver_port', '110', 'yes');
INSERT INTO `piano_options` VALUES (18, 'default_category', '1', 'yes');
INSERT INTO `piano_options` VALUES (19, 'default_comment_status', 'open', 'yes');
INSERT INTO `piano_options` VALUES (20, 'default_ping_status', 'open', 'yes');
INSERT INTO `piano_options` VALUES (21, 'default_pingback_flag', '1', 'yes');
INSERT INTO `piano_options` VALUES (22, 'posts_per_page', '10', 'yes');
INSERT INTO `piano_options` VALUES (23, 'date_format', 'F j, Y', 'yes');
INSERT INTO `piano_options` VALUES (24, 'time_format', 'g:i a', 'yes');
INSERT INTO `piano_options` VALUES (25, 'links_updated_date_format', 'F j, Y g:i a', 'yes');
INSERT INTO `piano_options` VALUES (26, 'comment_moderation', '0', 'yes');
INSERT INTO `piano_options` VALUES (27, 'moderation_notify', '1', 'yes');
INSERT INTO `piano_options` VALUES (28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes');
INSERT INTO `piano_options` VALUES (29, 'rewrite_rules', 'a:157:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=56&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes');
INSERT INTO `piano_options` VALUES (30, 'hack_file', '0', 'yes');
INSERT INTO `piano_options` VALUES (31, 'blog_charset', 'UTF-8', 'yes');
INSERT INTO `piano_options` VALUES (32, 'moderation_keys', '', 'no');
INSERT INTO `piano_options` VALUES (33, 'active_plugins', 'a:4:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:35:\"carousel-slider/carousel-slider.php\";i:2;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:3;s:27:\"woocommerce/woocommerce.php\";}', 'yes');
INSERT INTO `piano_options` VALUES (34, 'category_base', '', 'yes');
INSERT INTO `piano_options` VALUES (35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes');
INSERT INTO `piano_options` VALUES (36, 'comment_max_links', '2', 'yes');
INSERT INTO `piano_options` VALUES (37, 'gmt_offset', '0', 'yes');
INSERT INTO `piano_options` VALUES (38, 'default_email_category', '1', 'yes');
INSERT INTO `piano_options` VALUES (39, 'recently_edited', '', 'no');
INSERT INTO `piano_options` VALUES (40, 'template', 'storefront', 'yes');
INSERT INTO `piano_options` VALUES (41, 'stylesheet', 'Peocandy', 'yes');
INSERT INTO `piano_options` VALUES (42, 'comment_whitelist', '1', 'yes');
INSERT INTO `piano_options` VALUES (43, 'blacklist_keys', '', 'no');
INSERT INTO `piano_options` VALUES (44, 'comment_registration', '0', 'yes');
INSERT INTO `piano_options` VALUES (45, 'html_type', 'text/html', 'yes');
INSERT INTO `piano_options` VALUES (46, 'use_trackback', '0', 'yes');
INSERT INTO `piano_options` VALUES (47, 'default_role', 'subscriber', 'yes');
INSERT INTO `piano_options` VALUES (48, 'db_version', '38590', 'yes');
INSERT INTO `piano_options` VALUES (49, 'uploads_use_yearmonth_folders', '1', 'yes');
INSERT INTO `piano_options` VALUES (50, 'upload_path', '', 'yes');
INSERT INTO `piano_options` VALUES (51, 'blog_public', '1', 'yes');
INSERT INTO `piano_options` VALUES (52, 'default_link_category', '2', 'yes');
INSERT INTO `piano_options` VALUES (53, 'show_on_front', 'page', 'yes');
INSERT INTO `piano_options` VALUES (54, 'tag_base', '', 'yes');
INSERT INTO `piano_options` VALUES (55, 'show_avatars', '1', 'yes');
INSERT INTO `piano_options` VALUES (56, 'avatar_rating', 'G', 'yes');
INSERT INTO `piano_options` VALUES (57, 'upload_url_path', '', 'yes');
INSERT INTO `piano_options` VALUES (58, 'thumbnail_size_w', '150', 'yes');
INSERT INTO `piano_options` VALUES (59, 'thumbnail_size_h', '150', 'yes');
INSERT INTO `piano_options` VALUES (60, 'thumbnail_crop', '1', 'yes');
INSERT INTO `piano_options` VALUES (61, 'medium_size_w', '300', 'yes');
INSERT INTO `piano_options` VALUES (62, 'medium_size_h', '300', 'yes');
INSERT INTO `piano_options` VALUES (63, 'avatar_default', 'mystery', 'yes');
INSERT INTO `piano_options` VALUES (64, 'large_size_w', '1024', 'yes');
INSERT INTO `piano_options` VALUES (65, 'large_size_h', '1024', 'yes');
INSERT INTO `piano_options` VALUES (66, 'image_default_link_type', 'none', 'yes');
INSERT INTO `piano_options` VALUES (67, 'image_default_size', '', 'yes');
INSERT INTO `piano_options` VALUES (68, 'image_default_align', '', 'yes');
INSERT INTO `piano_options` VALUES (69, 'close_comments_for_old_posts', '0', 'yes');
INSERT INTO `piano_options` VALUES (70, 'close_comments_days_old', '14', 'yes');
INSERT INTO `piano_options` VALUES (71, 'thread_comments', '1', 'yes');
INSERT INTO `piano_options` VALUES (72, 'thread_comments_depth', '5', 'yes');
INSERT INTO `piano_options` VALUES (73, 'page_comments', '0', 'yes');
INSERT INTO `piano_options` VALUES (74, 'comments_per_page', '50', 'yes');
INSERT INTO `piano_options` VALUES (75, 'default_comments_page', 'newest', 'yes');
INSERT INTO `piano_options` VALUES (76, 'comment_order', 'asc', 'yes');
INSERT INTO `piano_options` VALUES (77, 'sticky_posts', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (81, 'uninstall_plugins', 'a:0:{}', 'no');
INSERT INTO `piano_options` VALUES (82, 'timezone_string', '', 'yes');
INSERT INTO `piano_options` VALUES (83, 'page_for_posts', '58', 'yes');
INSERT INTO `piano_options` VALUES (84, 'page_on_front', '56', 'yes');
INSERT INTO `piano_options` VALUES (85, 'default_post_format', '0', 'yes');
INSERT INTO `piano_options` VALUES (86, 'link_manager_enabled', '0', 'yes');
INSERT INTO `piano_options` VALUES (87, 'finished_splitting_shared_terms', '1', 'yes');
INSERT INTO `piano_options` VALUES (88, 'site_icon', '0', 'yes');
INSERT INTO `piano_options` VALUES (89, 'medium_large_size_w', '768', 'yes');
INSERT INTO `piano_options` VALUES (90, 'medium_large_size_h', '0', 'yes');
INSERT INTO `piano_options` VALUES (91, 'initial_db_version', '38590', 'yes');
INSERT INTO `piano_options` VALUES (92, 'piano_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes');
INSERT INTO `piano_options` VALUES (93, 'fresh_site', '0', 'yes');
INSERT INTO `piano_options` VALUES (94, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (99, 'sidebars_widgets', 'a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes');
INSERT INTO `piano_options` VALUES (100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (102, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (103, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (104, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (105, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (106, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (107, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (108, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (109, 'cron', 'a:12:{i:1527151796;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1527152615;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1527156202;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1527167002;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1527175798;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1527206400;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1527219009;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1527219670;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1527231802;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1527231812;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1528156800;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes');
INSERT INTO `piano_options` VALUES (116, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1527145394;s:7:\"checked\";a:2:{s:8:\"Peocandy\";s:5:\"1.0.0\";s:10:\"storefront\";s:5:\"2.3.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `piano_options` VALUES (120, 'can_compress_scripts', '1', 'no');
INSERT INTO `piano_options` VALUES (125, 'recently_activated', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (138, 'woocommerce_store_address', 'Số 2 ngách 24 Linh Quang Đống Đa', 'yes');
INSERT INTO `piano_options` VALUES (139, 'woocommerce_store_address_2', '', 'yes');
INSERT INTO `piano_options` VALUES (140, 'woocommerce_store_city', 'Hà Nội', 'yes');
INSERT INTO `piano_options` VALUES (141, 'woocommerce_default_country', 'VN', 'yes');
INSERT INTO `piano_options` VALUES (142, 'woocommerce_store_postcode', '10000', 'yes');
INSERT INTO `piano_options` VALUES (143, 'woocommerce_allowed_countries', 'all', 'yes');
INSERT INTO `piano_options` VALUES (144, 'woocommerce_all_except_countries', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (145, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (146, 'woocommerce_ship_to_countries', '', 'yes');
INSERT INTO `piano_options` VALUES (147, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (148, 'woocommerce_default_customer_address', 'geolocation', 'yes');
INSERT INTO `piano_options` VALUES (149, 'woocommerce_calc_taxes', 'no', 'yes');
INSERT INTO `piano_options` VALUES (150, 'woocommerce_currency', 'VND', 'yes');
INSERT INTO `piano_options` VALUES (151, 'woocommerce_currency_pos', 'left', 'yes');
INSERT INTO `piano_options` VALUES (152, 'woocommerce_price_thousand_sep', ',', 'yes');
INSERT INTO `piano_options` VALUES (153, 'woocommerce_price_decimal_sep', '.', 'yes');
INSERT INTO `piano_options` VALUES (154, 'woocommerce_price_num_decimals', '2', 'yes');
INSERT INTO `piano_options` VALUES (155, 'woocommerce_shop_page_id', '4', 'yes');
INSERT INTO `piano_options` VALUES (156, 'woocommerce_cart_redirect_after_add', 'no', 'yes');
INSERT INTO `piano_options` VALUES (157, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (158, 'woocommerce_weight_unit', 'kg', 'yes');
INSERT INTO `piano_options` VALUES (159, 'woocommerce_dimension_unit', 'm', 'yes');
INSERT INTO `piano_options` VALUES (160, 'woocommerce_enable_reviews', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (161, 'woocommerce_review_rating_verification_label', 'yes', 'no');
INSERT INTO `piano_options` VALUES (162, 'woocommerce_review_rating_verification_required', 'no', 'no');
INSERT INTO `piano_options` VALUES (163, 'woocommerce_enable_review_rating', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (164, 'woocommerce_review_rating_required', 'yes', 'no');
INSERT INTO `piano_options` VALUES (165, 'woocommerce_manage_stock', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (166, 'woocommerce_hold_stock_minutes', '60', 'no');
INSERT INTO `piano_options` VALUES (167, 'woocommerce_notify_low_stock', 'yes', 'no');
INSERT INTO `piano_options` VALUES (168, 'woocommerce_notify_no_stock', 'yes', 'no');
INSERT INTO `piano_options` VALUES (169, 'woocommerce_stock_email_recipient', 'hieuhaono@gmail.com', 'no');
INSERT INTO `piano_options` VALUES (170, 'woocommerce_notify_low_stock_amount', '2', 'no');
INSERT INTO `piano_options` VALUES (171, 'woocommerce_notify_no_stock_amount', '0', 'yes');
INSERT INTO `piano_options` VALUES (172, 'woocommerce_hide_out_of_stock_items', 'no', 'yes');
INSERT INTO `piano_options` VALUES (173, 'woocommerce_stock_format', '', 'yes');
INSERT INTO `piano_options` VALUES (174, 'woocommerce_file_download_method', 'force', 'no');
INSERT INTO `piano_options` VALUES (175, 'woocommerce_downloads_require_login', 'no', 'no');
INSERT INTO `piano_options` VALUES (176, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no');
INSERT INTO `piano_options` VALUES (177, 'woocommerce_prices_include_tax', 'no', 'yes');
INSERT INTO `piano_options` VALUES (178, 'woocommerce_tax_based_on', 'shipping', 'yes');
INSERT INTO `piano_options` VALUES (179, 'woocommerce_shipping_tax_class', 'inherit', 'yes');
INSERT INTO `piano_options` VALUES (180, 'woocommerce_tax_round_at_subtotal', 'no', 'yes');
INSERT INTO `piano_options` VALUES (181, 'woocommerce_tax_classes', 'Reduced rate\r\nZero rate', 'yes');
INSERT INTO `piano_options` VALUES (182, 'woocommerce_tax_display_shop', 'excl', 'yes');
INSERT INTO `piano_options` VALUES (183, 'woocommerce_tax_display_cart', 'excl', 'no');
INSERT INTO `piano_options` VALUES (184, 'woocommerce_price_display_suffix', '', 'yes');
INSERT INTO `piano_options` VALUES (185, 'woocommerce_tax_total_display', 'itemized', 'no');
INSERT INTO `piano_options` VALUES (186, 'woocommerce_enable_shipping_calc', 'yes', 'no');
INSERT INTO `piano_options` VALUES (187, 'woocommerce_shipping_cost_requires_address', 'no', 'no');
INSERT INTO `piano_options` VALUES (188, 'woocommerce_ship_to_destination', 'billing', 'no');
INSERT INTO `piano_options` VALUES (189, 'woocommerce_shipping_debug_mode', 'no', 'no');
INSERT INTO `piano_options` VALUES (190, 'woocommerce_enable_coupons', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (191, 'woocommerce_calc_discounts_sequentially', 'no', 'no');
INSERT INTO `piano_options` VALUES (192, 'woocommerce_enable_guest_checkout', 'yes', 'no');
INSERT INTO `piano_options` VALUES (193, 'woocommerce_force_ssl_checkout', 'no', 'yes');
INSERT INTO `piano_options` VALUES (194, 'woocommerce_unforce_ssl_checkout', 'no', 'yes');
INSERT INTO `piano_options` VALUES (195, 'woocommerce_cart_page_id', '5', 'yes');
INSERT INTO `piano_options` VALUES (196, 'woocommerce_checkout_page_id', '6', 'yes');
INSERT INTO `piano_options` VALUES (197, 'woocommerce_terms_page_id', '', 'no');
INSERT INTO `piano_options` VALUES (198, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes');
INSERT INTO `piano_options` VALUES (199, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes');
INSERT INTO `piano_options` VALUES (200, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes');
INSERT INTO `piano_options` VALUES (201, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes');
INSERT INTO `piano_options` VALUES (202, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes');
INSERT INTO `piano_options` VALUES (203, 'woocommerce_myaccount_page_id', '7', 'yes');
INSERT INTO `piano_options` VALUES (204, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no');
INSERT INTO `piano_options` VALUES (205, 'woocommerce_enable_myaccount_registration', 'no', 'no');
INSERT INTO `piano_options` VALUES (206, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no');
INSERT INTO `piano_options` VALUES (207, 'woocommerce_registration_generate_username', 'yes', 'no');
INSERT INTO `piano_options` VALUES (208, 'woocommerce_registration_generate_password', 'no', 'no');
INSERT INTO `piano_options` VALUES (209, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes');
INSERT INTO `piano_options` VALUES (210, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes');
INSERT INTO `piano_options` VALUES (211, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes');
INSERT INTO `piano_options` VALUES (212, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes');
INSERT INTO `piano_options` VALUES (213, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes');
INSERT INTO `piano_options` VALUES (214, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes');
INSERT INTO `piano_options` VALUES (215, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes');
INSERT INTO `piano_options` VALUES (216, 'woocommerce_logout_endpoint', 'customer-logout', 'yes');
INSERT INTO `piano_options` VALUES (217, 'woocommerce_email_from_name', 'Piano', 'no');
INSERT INTO `piano_options` VALUES (218, 'woocommerce_email_from_address', 'hieuhaono@gmail.com', 'no');
INSERT INTO `piano_options` VALUES (219, 'woocommerce_email_header_image', '', 'no');
INSERT INTO `piano_options` VALUES (220, 'woocommerce_email_footer_text', '{site_title}', 'no');
INSERT INTO `piano_options` VALUES (221, 'woocommerce_email_base_color', '#96588a', 'no');
INSERT INTO `piano_options` VALUES (222, 'woocommerce_email_background_color', '#f7f7f7', 'no');
INSERT INTO `piano_options` VALUES (223, 'woocommerce_email_body_background_color', '#ffffff', 'no');
INSERT INTO `piano_options` VALUES (224, 'woocommerce_email_text_color', '#3c3c3c', 'no');
INSERT INTO `piano_options` VALUES (225, 'woocommerce_api_enabled', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (226, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes');
INSERT INTO `piano_options` VALUES (227, 'current_theme_supports_woocommerce', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (228, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes');
INSERT INTO `piano_options` VALUES (231, 'default_product_cat', '15', 'yes');
INSERT INTO `piano_options` VALUES (236, 'woocommerce_admin_notices', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (237, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (238, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (239, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (240, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (241, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (242, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (243, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (244, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (245, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (246, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (247, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (248, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (249, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (252, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (253, 'woocommerce_product_type', 'physical', 'yes');
INSERT INTO `piano_options` VALUES (254, 'woocommerce_allow_tracking', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (255, 'woocommerce_tracker_last_send', '1527132649', 'yes');
INSERT INTO `piano_options` VALUES (257, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:\"reroute_requests\";b:0;s:5:\"email\";s:19:\"hieuhaono@gmail.com\";}', 'yes');
INSERT INTO `piano_options` VALUES (258, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes');
INSERT INTO `piano_options` VALUES (259, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes');
INSERT INTO `piano_options` VALUES (260, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:3:\"yes\";}', 'yes');
INSERT INTO `piano_options` VALUES (261, 'wc_ppec_version', '1.5.4', 'yes');
INSERT INTO `piano_options` VALUES (267, '_transient_shipping-transient-version', '1526441498', 'yes');
INSERT INTO `piano_options` VALUES (268, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:4:\"3000\";}', 'yes');
INSERT INTO `piano_options` VALUES (269, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:4:\"3000\";}', 'yes');
INSERT INTO `piano_options` VALUES (270, 'theme_mods_twentyseventeen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1526441566;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes');
INSERT INTO `piano_options` VALUES (271, 'current_theme', 'Storefront Child', 'yes');
INSERT INTO `piano_options` VALUES (272, 'theme_mods_storefront', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1526442039;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}}}}', 'yes');
INSERT INTO `piano_options` VALUES (273, 'theme_switched', '', 'yes');
INSERT INTO `piano_options` VALUES (274, 'storefront_nux_fresh_site', '0', 'yes');
INSERT INTO `piano_options` VALUES (275, 'woocommerce_catalog_rows', '4', 'yes');
INSERT INTO `piano_options` VALUES (276, 'woocommerce_catalog_columns', '3', 'yes');
INSERT INTO `piano_options` VALUES (277, 'woocommerce_maybe_regenerate_images_hash', '27acde77266b4d2a3491118955cb3f66', 'yes');
INSERT INTO `piano_options` VALUES (281, 'storefront_nux_dismissed', '1', 'yes');
INSERT INTO `piano_options` VALUES (282, '_transient_product_query-transient-version', '1527147011', 'yes');
INSERT INTO `piano_options` VALUES (283, '_transient_product-transient-version', '1527147011', 'yes');
INSERT INTO `piano_options` VALUES (293, '_transient_wc_attribute_taxonomies', 'a:2:{i:0;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"1\";s:14:\"attribute_name\";s:5:\"color\";s:15:\"attribute_label\";s:5:\"Color\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}i:1;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"2\";s:14:\"attribute_name\";s:4:\"size\";s:15:\"attribute_label\";s:4:\"Size\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}}', 'yes');
INSERT INTO `piano_options` VALUES (299, 'pa_size_children', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (329, '_transient_timeout_wc_product_children_10', '1529034032', 'no');
INSERT INTO `piano_options` VALUES (330, '_transient_wc_product_children_10', 'a:2:{s:3:\"all\";a:4:{i:0;i:33;i:1;i:26;i:2;i:27;i:3;i:28;}s:7:\"visible\";a:4:{i:0;i:33;i:1;i:26;i:2;i:27;i:3;i:28;}}', 'no');
INSERT INTO `piano_options` VALUES (331, '_transient_timeout_wc_var_prices_10', '1529739016', 'no');
INSERT INTO `piano_options` VALUES (332, '_transient_wc_var_prices_10', '{\"version\":\"1527147011\",\"2938fdb4257fe9d0e0c7f3b2a463c17f\":{\"price\":{\"33\":\"45.00\",\"26\":\"42.00\",\"27\":\"45.00\",\"28\":\"45.00\"},\"regular_price\":{\"33\":\"45.00\",\"26\":\"45.00\",\"27\":\"45.00\",\"28\":\"45.00\"},\"sale_price\":{\"33\":\"45.00\",\"26\":\"42.00\",\"27\":\"45.00\",\"28\":\"45.00\"}}}', 'no');
INSERT INTO `piano_options` VALUES (333, '_transient_timeout_wc_product_children_9', '1529034032', 'no');
INSERT INTO `piano_options` VALUES (334, '_transient_wc_product_children_9', 'a:2:{s:3:\"all\";a:3:{i:0;i:23;i:1;i:24;i:2;i:25;}s:7:\"visible\";a:3:{i:0;i:23;i:1;i:24;i:2;i:25;}}', 'no');
INSERT INTO `piano_options` VALUES (335, '_transient_timeout_wc_var_prices_9', '1529739016', 'no');
INSERT INTO `piano_options` VALUES (336, '_transient_wc_var_prices_9', '{\"version\":\"1527147011\",\"2938fdb4257fe9d0e0c7f3b2a463c17f\":{\"price\":{\"23\":\"20.00\",\"24\":\"20.00\",\"25\":\"15.00\"},\"regular_price\":{\"23\":\"20.00\",\"24\":\"20.00\",\"25\":\"15.00\"},\"sale_price\":{\"23\":\"20.00\",\"24\":\"20.00\",\"25\":\"15.00\"}}}', 'no');
INSERT INTO `piano_options` VALUES (338, 'theme_mods_Peocandy', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:3:{s:7:\"primary\";i:33;s:9:\"secondary\";i:30;s:18:\"home_page_category\";i:41;}s:18:\"custom_css_post_id\";i:-1;}', 'yes');
INSERT INTO `piano_options` VALUES (362, 'woocommerce_tracker_ua', 'a:2:{i:0;s:115:\"mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/66.0.3359.139 safari/537.36\";i:1;s:115:\"mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/66.0.3359.181 safari/537.36\";}', 'yes');
INSERT INTO `piano_options` VALUES (387, '_site_transient_timeout_browser_37852581c9557110c7bab064af517413', '1527494019', 'no');
INSERT INTO `piano_options` VALUES (388, '_site_transient_browser_37852581c9557110c7bab064af517413', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"66.0.3359.181\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no');
INSERT INTO `piano_options` VALUES (411, 'carousel_slider_version', '1.8.8', 'yes');
INSERT INTO `piano_options` VALUES (412, 'widget_widget_carousel_slider', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `piano_options` VALUES (418, 'storefront_nux_guided_tour', '1', 'yes');
INSERT INTO `piano_options` VALUES (424, '_transient_timeout_wc_report_sales_by_date', '1527222509', 'no');
INSERT INTO `piano_options` VALUES (425, '_transient_wc_report_sales_by_date', 'a:64:{s:32:\"21737051973ee297d90b611221086fea\";a:0:{}s:32:\"ae468d09cdfa27068a29098fa1f00f42\";a:0:{}s:32:\"8843660e0b15ddb51c9ac18a5a8e39b0\";a:0:{}s:32:\"651289322015da7a1c76e395b3efaeee\";N;s:32:\"f4b9ef800c7c5a21ccd15b2d8bc518d9\";a:0:{}s:32:\"0b1491dcd9543567c08054c542949ffd\";a:0:{}s:32:\"09cd43c443ddefbaf4d095e0f96056ee\";a:0:{}s:32:\"66c321626c9ed4ef6caeef98372cb128\";a:0:{}s:32:\"12471085360e555d4d844768764f3161\";a:0:{}s:32:\"ccc758c648118aef3a58f7c5c191d022\";a:0:{}s:32:\"971a92b03707b30d2fe2416e0fee35ab\";a:0:{}s:32:\"264d353c7648d3eb0b1b978fde67c3e8\";N;s:32:\"ab4384fa6ad633b32adc4e486648a0ea\";a:0:{}s:32:\"d7a9fd132d3c4568be5f8ef3b662cd61\";a:0:{}s:32:\"e1bdff6874c7b4ce5ef928cfd523fe5f\";a:0:{}s:32:\"31505b60c4a34cbfa855a43d719dc254\";a:0:{}s:32:\"4a3cd18548bc34bb1ab5e24dd47fe0ef\";a:0:{}s:32:\"90b842ba45a5b3ae63103925df1ae0ee\";a:0:{}s:32:\"db9a01aa66666af732566f072a0ce380\";a:0:{}s:32:\"14be2311d6d8af3c632cddf271477578\";N;s:32:\"55fed037d6f2a34f195aa43e82dcf928\";a:0:{}s:32:\"c1fabe4c4cf8fc3a3fe996c76fd0a03c\";a:0:{}s:32:\"8e6b0f2f8f5b67259772156fc2d729b2\";a:0:{}s:32:\"872b4e965bfd9c7174965ddfccc12d15\";a:0:{}s:32:\"8ee36b78512136a4a7d8ae92ab04ebab\";a:0:{}s:32:\"99c988f3978bcabdd162fa541ea6197a\";a:0:{}s:32:\"87a7cae2e0a5781218537c9bd91af795\";a:0:{}s:32:\"4ad4a3466fd941a223cdcc9a281db192\";N;s:32:\"3a242b89cdac5988907f9c22df046cb9\";a:0:{}s:32:\"fe0d44c97a30d58fbf1433632bf80dcb\";a:0:{}s:32:\"9d290f1d15cfaab1767592c5bbbe42ab\";a:0:{}s:32:\"1c2007c10b0108390b7c2726cad507e3\";a:0:{}s:32:\"6d5bb5d7942a9da0d61b62307c3b568f\";a:0:{}s:32:\"de7cd72657a4f2bdcd09443cc43e37da\";a:0:{}s:32:\"1830872ddac734f806c5f261a5328bac\";a:0:{}s:32:\"902442470ce719bb81c37c162d5c3318\";N;s:32:\"06d225a132e080fa2f3abe4ceace8a73\";a:0:{}s:32:\"400a8c5056b28b8d76c6c03fd1c25eca\";a:0:{}s:32:\"07c1773a31cb818543a59d4e0ec013ec\";a:0:{}s:32:\"9f45ee76d9be074b35d14f067d4a32f0\";a:0:{}s:32:\"65c2d1b0f3c2899f7594fbb450b9505d\";a:0:{}s:32:\"2ee848866e1b7783a0beeca4bd355229\";a:0:{}s:32:\"b54d6da8e3c096c42aa620cb751817f1\";a:0:{}s:32:\"fe63b363fe14b3db25a7bc887d1b51e4\";N;s:32:\"17b249153783acc5037c0231cdc91b7c\";a:0:{}s:32:\"52f523ec5f03db8e5ae441ef0d230e6a\";a:0:{}s:32:\"963d24927c7f0e9affe1bdb50e23c4f4\";a:0:{}s:32:\"004acb07a877787a9ba2d642df0c78c5\";a:0:{}s:32:\"2e57186197e56548265b6c5f30333879\";a:0:{}s:32:\"2e1bc94dbd1a45b2d8af8635517fe96b\";a:0:{}s:32:\"d8422ee53054aa81bd569612ec4c5d4b\";a:0:{}s:32:\"6fc069a0a929748f0ab9b4bdb9bc403b\";N;s:32:\"7b389ae2f0a3e76c0343ab59466e3d69\";a:0:{}s:32:\"e85a9de0248487933fbb75c39a54f38f\";a:0:{}s:32:\"55058454e07fb0dd72558b4a1835e8ae\";a:0:{}s:32:\"7cc054c2948f39ff1ae9afc16f73811d\";a:0:{}s:32:\"dab386b4364febdbf460c79aa5b23c24\";a:0:{}s:32:\"6e293c70688ce65523d04f923b49cc36\";a:0:{}s:32:\"a5414984833610c293acd2aa56cd2102\";a:0:{}s:32:\"79e793857dc31833ea7c2a0ff179ca3e\";N;s:32:\"3f8e6bb5951ee6aa6af42b471625a956\";a:0:{}s:32:\"901e615e940bffa528e3c9c294f0ba42\";a:0:{}s:32:\"cf521e63cf7101e77bd0bc10832ab30e\";a:0:{}s:32:\"31dc737783970391f6785b0f62894f47\";a:0:{}}', 'no');
INSERT INTO `piano_options` VALUES (445, '_transient_timeout_plugin_slugs', '1527150875', 'no');
INSERT INTO `piano_options` VALUES (446, '_transient_plugin_slugs', 'a:9:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:19:\"akismet/akismet.php\";i:2;s:35:\"carousel-slider/carousel-slider.php\";i:3;s:9:\"hello.php\";i:4;s:19:\"jetpack/jetpack.php\";i:5;s:23:\"ml-slider/ml-slider.php\";i:6;s:27:\"woocommerce/woocommerce.php\";i:7;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:8;s:41:\"wordpress-importer/wordpress-importer.php\";}', 'no');
INSERT INTO `piano_options` VALUES (447, 'acf_version', '5.6.9', 'yes');
INSERT INTO `piano_options` VALUES (454, 'category_children', 'a:0:{}', 'yes');
INSERT INTO `piano_options` VALUES (455, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes');
INSERT INTO `piano_options` VALUES (461, '_transient_timeout_external_ip_address_::1', '1527651242', 'no');
INSERT INTO `piano_options` VALUES (462, '_transient_external_ip_address_::1', '118.70.129.41', 'no');
INSERT INTO `piano_options` VALUES (477, 'options_menu_top_email', 'hieuhaono@gmail.com', 'no');
INSERT INTO `piano_options` VALUES (478, '_options_menu_top_email', 'field_5b0528123acd9', 'no');
INSERT INTO `piano_options` VALUES (479, 'options_menu_top_phone', '01275195757', 'no');
INSERT INTO `piano_options` VALUES (480, '_options_menu_top_phone', 'field_5b05283b3acda', 'no');
INSERT INTO `piano_options` VALUES (481, 'options_menu_top_hotline', '0912419680', 'no');
INSERT INTO `piano_options` VALUES (482, '_options_menu_top_hotline', 'field_5b0528663acdb', 'no');
INSERT INTO `piano_options` VALUES (485, '_transient_timeout_wc_shipping_method_count_1_1526441498', '1529661703', 'no');
INSERT INTO `piano_options` VALUES (486, '_transient_wc_shipping_method_count_1_1526441498', '2', 'no');
INSERT INTO `piano_options` VALUES (493, '_site_transient_timeout_community-events-4501c091b0366d76ea3218b6cfdd8097', '1527171230', 'no');
INSERT INTO `piano_options` VALUES (494, '_site_transient_community-events-4501c091b0366d76ea3218b6cfdd8097', 'a:2:{s:8:\"location\";a:1:{s:2:\"ip\";s:2:\"::\";}s:6:\"events\";a:2:{i:0;a:7:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:15:\"WordCamp Europe\";s:3:\"url\";s:33:\"https://2018.europe.wordcamp.org/\";s:6:\"meetup\";s:0:\"\";s:10:\"meetup_url\";s:0:\"\";s:4:\"date\";s:19:\"2018-06-14 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:16:\"Belgrade, Serbia\";s:7:\"country\";s:2:\"RS\";s:8:\"latitude\";d:44.808497;s:9:\"longitude\";d:20.432285;}}i:1;a:7:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:4:\"WP15\";s:3:\"url\";s:86:\"https://wordpress.org/news/2018/04/celebrate-the-wordpress-15th-anniversary-on-may-27/\";s:6:\"meetup\";N;s:10:\"meetup_url\";N;s:4:\"date\";s:19:\"2018-05-27 12:00:00\";s:8:\"location\";a:1:{s:8:\"location\";s:10:\"Everywhere\";}}}}', 'no');
INSERT INTO `piano_options` VALUES (495, 'options_service_0_service_name', 'Bảo hành 3 năm cho tất cả sản phẩm', 'no');
INSERT INTO `piano_options` VALUES (496, '_options_service_0_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (497, 'options_service_0_service_icon', '98', 'no');
INSERT INTO `piano_options` VALUES (498, '_options_service_0_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (499, 'options_service_0_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (500, '_options_service_0_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (501, 'options_service_1_service_name', 'Miễn phí giao hàng khu vực nội thành', 'no');
INSERT INTO `piano_options` VALUES (502, '_options_service_1_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (503, 'options_service_1_service_icon', '99', 'no');
INSERT INTO `piano_options` VALUES (504, '_options_service_1_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (505, 'options_service_1_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (506, '_options_service_1_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (507, 'options_service_2_service_name', 'Hỗ trợ tư vấn 24/7', 'no');
INSERT INTO `piano_options` VALUES (508, '_options_service_2_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (509, 'options_service_2_service_icon', '100', 'no');
INSERT INTO `piano_options` VALUES (510, '_options_service_2_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (511, 'options_service_2_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (512, '_options_service_2_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (513, 'options_service_3_service_name', 'Đại lý piano lớn nhất Miền Bắc', 'no');
INSERT INTO `piano_options` VALUES (514, '_options_service_3_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (515, 'options_service_3_service_icon', '101', 'no');
INSERT INTO `piano_options` VALUES (516, '_options_service_3_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (517, 'options_service_3_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (518, '_options_service_3_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (519, 'options_service', '4', 'no');
INSERT INTO `piano_options` VALUES (520, '_options_service', 'field_5b062095e6faf', 'no');
INSERT INTO `piano_options` VALUES (532, '_transient_timeout_wc_admin_report', '1527222509', 'no');
INSERT INTO `piano_options` VALUES (533, '_transient_wc_admin_report', 'a:1:{s:32:\"3cb764c7082c1f655dfb2ee0d9a5ca9a\";a:0:{}}', 'no');
INSERT INTO `piano_options` VALUES (534, 'options_category_content_0_number_cat_dis', '5', 'no');
INSERT INTO `piano_options` VALUES (535, '_options_category_content_0_number_cat_dis', 'field_5b063f8ee2494', 'no');
INSERT INTO `piano_options` VALUES (536, 'options_category_content_0_number_colum', '5', 'no');
INSERT INTO `piano_options` VALUES (537, '_options_category_content_0_number_colum', 'field_5b063fd8e2495', 'no');
INSERT INTO `piano_options` VALUES (538, 'options_category_content_0_title_display', 'Danh mục sản phẩm', 'no');
INSERT INTO `piano_options` VALUES (539, '_options_category_content_0_title_display', 'field_5b06400ae2496', 'no');
INSERT INTO `piano_options` VALUES (540, 'options_category_content', '1', 'no');
INSERT INTO `piano_options` VALUES (541, '_options_category_content', 'field_5b063f7ae2493', 'no');
INSERT INTO `piano_options` VALUES (543, 'options_service_content', '4', 'no');
INSERT INTO `piano_options` VALUES (544, '_options_service_content', 'field_5b062095e6faf', 'no');
INSERT INTO `piano_options` VALUES (545, 'options_category_content_0_number_child_cat', '0', 'no');
INSERT INTO `piano_options` VALUES (546, '_options_category_content_0_number_child_cat', 'field_5b06459d7b9d3', 'no');
INSERT INTO `piano_options` VALUES (554, '_site_transient_timeout_theme_roots', '1527146295', 'no');
INSERT INTO `piano_options` VALUES (555, '_site_transient_theme_roots', 'a:2:{s:8:\"Peocandy\";s:7:\"/themes\";s:10:\"storefront\";s:7:\"/themes\";}', 'no');
INSERT INTO `piano_options` VALUES (556, '_transient_timeout__woocommerce_helper_updates', '1527187695', 'no');
INSERT INTO `piano_options` VALUES (557, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1527144495;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no');
INSERT INTO `piano_options` VALUES (560, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.6.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.6.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.6-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.6-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.6\";s:7:\"version\";s:5:\"4.9.6\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1527145392;s:15:\"version_checked\";s:5:\"4.9.6\";s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `piano_options` VALUES (566, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1527145397;s:7:\"checked\";a:9:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.6.9\";s:19:\"akismet/akismet.php\";s:5:\"4.0.3\";s:35:\"carousel-slider/carousel-slider.php\";s:5:\"1.8.8\";s:9:\"hello.php\";s:3:\"1.7\";s:19:\"jetpack/jetpack.php\";s:3:\"6.1\";s:23:\"ml-slider/ml-slider.php\";s:5:\"3.7.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.4.0\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:5:\"1.5.4\";s:41:\"wordpress-importer/wordpress-importer.php\";s:5:\"0.6.4\";}s:8:\"response\";a:3:{s:19:\"jetpack/jetpack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/jetpack\";s:4:\"slug\";s:7:\"jetpack\";s:6:\"plugin\";s:19:\"jetpack/jetpack.php\";s:11:\"new_version\";s:5:\"6.1.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/jetpack/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/jetpack.6.1.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/jetpack/assets/icon-256x256.png?rev=1791404\";s:2:\"1x\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";s:3:\"svg\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/jetpack/assets/banner-1544x500.png?rev=1791404\";s:2:\"1x\";s:62:\"https://ps.w.org/jetpack/assets/banner-772x250.png?rev=1791404\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.6\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:57:\"w.org/plugins/woocommerce-gateway-paypal-express-checkout\";s:4:\"slug\";s:43:\"woocommerce-gateway-paypal-express-checkout\";s:6:\"plugin\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:11:\"new_version\";s:5:\"1.5.5\";s:3:\"url\";s:74:\"https://wordpress.org/plugins/woocommerce-gateway-paypal-express-checkout/\";s:7:\"package\";s:92:\"https://downloads.wordpress.org/plugin/woocommerce-gateway-paypal-express-checkout.1.5.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-256x256.png?rev=1410389\";s:2:\"1x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-128x128.png?rev=1410389\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:99:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-1544x500.png?rev=1410389\";s:2:\"1x\";s:98:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-772x250.png?rev=1410389\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.0\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.6.10\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:5:\"4.9.9\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:6:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"carousel-slider/carousel-slider.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/carousel-slider\";s:4:\"slug\";s:15:\"carousel-slider\";s:6:\"plugin\";s:35:\"carousel-slider/carousel-slider.php\";s:11:\"new_version\";s:5:\"1.8.8\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/carousel-slider/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/carousel-slider.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:60:\"https://ps.w.org/carousel-slider/assets/icon.svg?rev=1727413\";s:3:\"svg\";s:60:\"https://ps.w.org/carousel-slider/assets/icon.svg?rev=1727413\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/carousel-slider/assets/banner-1544x500.jpg?rev=1746085\";s:2:\"1x\";s:70:\"https://ps.w.org/carousel-slider/assets/banner-772x250.jpg?rev=1746085\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"ml-slider/ml-slider.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/ml-slider\";s:4:\"slug\";s:9:\"ml-slider\";s:6:\"plugin\";s:23:\"ml-slider/ml-slider.php\";s:11:\"new_version\";s:5:\"3.7.2\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/ml-slider/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/ml-slider.3.7.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:62:\"https://ps.w.org/ml-slider/assets/icon-256x256.png?rev=1837669\";s:2:\"1x\";s:54:\"https://ps.w.org/ml-slider/assets/icon.svg?rev=1837669\";s:3:\"svg\";s:54:\"https://ps.w.org/ml-slider/assets/icon.svg?rev=1837669\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/ml-slider/assets/banner-1544x500.png?rev=1837669\";s:2:\"1x\";s:64:\"https://ps.w.org/ml-slider/assets/banner-772x250.png?rev=1837669\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.4.0\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.4.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"wordpress-importer/wordpress-importer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wordpress-importer\";s:4:\"slug\";s:18:\"wordpress-importer\";s:6:\"plugin\";s:41:\"wordpress-importer/wordpress-importer.php\";s:11:\"new_version\";s:5:\"0.6.4\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wordpress-importer/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/wordpress-importer.0.6.4.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:69:\"https://s.w.org/plugins/geopattern-icon/wordpress-importer_5696b3.svg\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-importer/assets/banner-772x250.png?rev=547654\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');
INSERT INTO `piano_options` VALUES (569, 'woocommerce_erasure_request_removes_order_data', 'no', 'no');
INSERT INTO `piano_options` VALUES (570, 'woocommerce_erasure_request_removes_download_data', 'no', 'no');
INSERT INTO `piano_options` VALUES (571, 'wp_page_for_privacy_policy', '', 'yes');
INSERT INTO `piano_options` VALUES (572, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes');
INSERT INTO `piano_options` VALUES (573, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes');
INSERT INTO `piano_options` VALUES (574, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no');
INSERT INTO `piano_options` VALUES (575, 'woocommerce_trash_pending_orders', '', 'no');
INSERT INTO `piano_options` VALUES (576, 'woocommerce_trash_failed_orders', '', 'no');
INSERT INTO `piano_options` VALUES (577, 'woocommerce_trash_cancelled_orders', '', 'no');
INSERT INTO `piano_options` VALUES (578, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no');
INSERT INTO `piano_options` VALUES (579, 'woocommerce_single_image_width', '600', 'yes');
INSERT INTO `piano_options` VALUES (580, 'woocommerce_thumbnail_image_width', '300', 'yes');
INSERT INTO `piano_options` VALUES (581, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes');
INSERT INTO `piano_options` VALUES (582, 'woocommerce_demo_store', 'no', 'no');
INSERT INTO `piano_options` VALUES (583, 'woocommerce_version', '3.4.0', 'yes');
INSERT INTO `piano_options` VALUES (593, 'woocommerce_db_version', '3.4.0', 'yes');
INSERT INTO `piano_options` VALUES (598, 'options_service_content_0_service_name', 'Bảo hành 3 năm cho tất cả sản phẩm', 'no');
INSERT INTO `piano_options` VALUES (599, '_options_service_content_0_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (600, 'options_service_content_0_service_icon', '98', 'no');
INSERT INTO `piano_options` VALUES (601, '_options_service_content_0_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (602, 'options_service_content_0_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (603, '_options_service_content_0_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (604, 'options_service_content_1_service_name', 'Miễn phí giao hàng khu vực nội thành', 'no');
INSERT INTO `piano_options` VALUES (605, '_options_service_content_1_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (606, 'options_service_content_1_service_icon', '99', 'no');
INSERT INTO `piano_options` VALUES (607, '_options_service_content_1_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (608, 'options_service_content_1_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (609, '_options_service_content_1_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (610, 'options_service_content_2_service_name', 'Hỗ trợ tư vấn 24/7', 'no');
INSERT INTO `piano_options` VALUES (611, '_options_service_content_2_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (612, 'options_service_content_2_service_icon', '100', 'no');
INSERT INTO `piano_options` VALUES (613, '_options_service_content_2_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (614, 'options_service_content_2_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (615, '_options_service_content_2_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (616, 'options_service_content_3_service_name', 'Đại lý piano lớn nhất Miền Bắc', 'no');
INSERT INTO `piano_options` VALUES (617, '_options_service_content_3_service_name', 'field_5b0620b8e6fb0', 'no');
INSERT INTO `piano_options` VALUES (618, 'options_service_content_3_service_icon', '101', 'no');
INSERT INTO `piano_options` VALUES (619, '_options_service_content_3_service_icon', 'field_5b0620cbe6fb1', 'no');
INSERT INTO `piano_options` VALUES (620, 'options_service_content_3_service_link', 'http://localhost/wordpressWoo', 'no');
INSERT INTO `piano_options` VALUES (621, '_options_service_content_3_service_link', 'field_5b0620e7e6fb2', 'no');
INSERT INTO `piano_options` VALUES (623, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes');
INSERT INTO `piano_options` VALUES (624, 'product_cat_children', 'a:1:{i:16;a:3:{i:0;i:17;i:1;i:18;i:2;i:19;}}', 'yes');
INSERT INTO `piano_options` VALUES (625, '_transient_is_multi_author', '0', 'yes');
INSERT INTO `piano_options` VALUES (626, '_transient_timeout_wc_term_counts', '1529739490', 'no');
INSERT INTO `piano_options` VALUES (627, '_transient_wc_term_counts', 'a:10:{i:39;s:1:\"1\";i:36;s:1:\"1\";i:38;s:1:\"1\";i:40;s:1:\"1\";i:34;s:1:\"1\";i:37;s:1:\"1\";i:35;s:1:\"1\";i:18;s:1:\"3\";i:17;s:1:\"5\";i:19;s:1:\"5\";}', 'no');
INSERT INTO `piano_options` VALUES (628, '_transient_timeout_wc_product_loop42d91527147011', '1529739489', 'no');
INSERT INTO `piano_options` VALUES (629, '_transient_wc_product_loop42d91527147011', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:32;i:1;i:31;i:2;i:30;i:3;i:29;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no');
INSERT INTO `piano_options` VALUES (630, '_transient_timeout_wc_product_loop78d71527147011', '1529739490', 'no');
INSERT INTO `piano_options` VALUES (631, '_transient_wc_product_loop78d71527147011', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:18;i:1;i:16;i:2;i:15;i:3;i:9;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no');
INSERT INTO `piano_options` VALUES (632, '_transient_timeout_wc_product_loopf8571527147011', '1529739490', 'no');
INSERT INTO `piano_options` VALUES (633, '_transient_wc_product_loopf8571527147011', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:0:{}s:5:\"total\";i:0;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no');
INSERT INTO `piano_options` VALUES (634, '_transient_timeout_wc_products_onsale', '1529739490', 'no');
INSERT INTO `piano_options` VALUES (635, '_transient_wc_products_onsale', 'a:8:{i:0;i:13;i:1;i:14;i:2;i:15;i:3;i:17;i:4;i:22;i:5;i:26;i:6;i:30;i:7;i:10;}', 'no');
INSERT INTO `piano_options` VALUES (636, '_transient_timeout_wc_product_loop50de1527147011', '1529739490', 'no');
INSERT INTO `piano_options` VALUES (637, '_transient_wc_product_loop50de1527147011', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:13;i:1;i:30;i:2;i:14;i:3;i:15;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no');
INSERT INTO `piano_options` VALUES (638, '_transient_timeout_wc_product_loopa47f1527147011', '1529739490', 'no');
INSERT INTO `piano_options` VALUES (639, '_transient_wc_product_loopa47f1527147011', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:21;i:1;i:22;i:2;i:9;i:3;i:29;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no');

-- ----------------------------
-- Table structure for piano_postmeta
-- ----------------------------
DROP TABLE IF EXISTS `piano_postmeta`;
CREATE TABLE `piano_postmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1342 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_postmeta
-- ----------------------------
INSERT INTO `piano_postmeta` VALUES (1, 2, '_wp_page_template', 'default');
INSERT INTO `piano_postmeta` VALUES (4, 9, '_sku', 'woo-vneck-tee');
INSERT INTO `piano_postmeta` VALUES (5, 9, '_regular_price', '');
INSERT INTO `piano_postmeta` VALUES (6, 9, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (7, 9, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (8, 9, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (9, 9, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (10, 9, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (11, 9, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (12, 9, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (13, 9, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (14, 9, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (15, 9, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (16, 9, '_length', '');
INSERT INTO `piano_postmeta` VALUES (17, 9, '_width', '');
INSERT INTO `piano_postmeta` VALUES (18, 9, '_height', '');
INSERT INTO `piano_postmeta` VALUES (19, 9, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (20, 9, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (21, 9, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (22, 9, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (23, 9, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (24, 9, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (25, 9, '_product_image_gallery', '35,36');
INSERT INTO `piano_postmeta` VALUES (26, 9, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (27, 9, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (28, 9, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (29, 9, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (30, 9, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (31, 9, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (32, 9, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (33, 9, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (34, 9, '_product_attributes', 'a:2:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:1;}s:7:\"pa_size\";a:6:{s:4:\"name\";s:7:\"pa_size\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (35, 9, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (38, 10, '_sku', 'woo-hoodie');
INSERT INTO `piano_postmeta` VALUES (39, 10, '_regular_price', '');
INSERT INTO `piano_postmeta` VALUES (40, 10, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (41, 10, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (42, 10, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (43, 10, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (44, 10, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (45, 10, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (46, 10, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (47, 10, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (48, 10, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (49, 10, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (50, 10, '_length', '');
INSERT INTO `piano_postmeta` VALUES (51, 10, '_width', '');
INSERT INTO `piano_postmeta` VALUES (52, 10, '_height', '');
INSERT INTO `piano_postmeta` VALUES (53, 10, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (54, 10, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (55, 10, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (56, 10, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (57, 10, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (58, 10, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (59, 10, '_product_image_gallery', '38,39,40');
INSERT INTO `piano_postmeta` VALUES (60, 10, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (61, 10, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (62, 10, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (63, 10, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (64, 10, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (65, 10, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (66, 10, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (67, 10, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (68, 10, '_product_attributes', 'a:2:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:1;}s:4:\"logo\";a:6:{s:4:\"name\";s:4:\"Logo\";s:5:\"value\";s:8:\"Yes | No\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:0;}}');
INSERT INTO `piano_postmeta` VALUES (69, 10, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (72, 11, '_sku', 'woo-hoodie-with-logo');
INSERT INTO `piano_postmeta` VALUES (73, 11, '_regular_price', '45');
INSERT INTO `piano_postmeta` VALUES (74, 11, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (75, 11, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (76, 11, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (77, 11, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (78, 11, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (79, 11, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (80, 11, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (81, 11, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (82, 11, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (83, 11, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (84, 11, '_length', '');
INSERT INTO `piano_postmeta` VALUES (85, 11, '_width', '');
INSERT INTO `piano_postmeta` VALUES (86, 11, '_height', '');
INSERT INTO `piano_postmeta` VALUES (87, 11, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (88, 11, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (89, 11, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (90, 11, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (91, 11, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (92, 11, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (93, 11, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (94, 11, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (95, 11, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (96, 11, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (97, 11, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (98, 11, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (99, 11, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (100, 11, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (101, 11, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (102, 11, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (103, 11, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (104, 11, '_price', '45');
INSERT INTO `piano_postmeta` VALUES (106, 12, '_sku', 'woo-tshirt');
INSERT INTO `piano_postmeta` VALUES (107, 12, '_regular_price', '18');
INSERT INTO `piano_postmeta` VALUES (108, 12, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (109, 12, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (110, 12, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (111, 12, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (112, 12, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (113, 12, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (114, 12, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (115, 12, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (116, 12, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (117, 12, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (118, 12, '_length', '');
INSERT INTO `piano_postmeta` VALUES (119, 12, '_width', '');
INSERT INTO `piano_postmeta` VALUES (120, 12, '_height', '');
INSERT INTO `piano_postmeta` VALUES (121, 12, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (122, 12, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (123, 12, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (124, 12, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (125, 12, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (126, 12, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (127, 12, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (128, 12, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (129, 12, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (130, 12, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (131, 12, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (132, 12, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (133, 12, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (134, 12, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (135, 12, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (136, 12, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (137, 12, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (138, 12, '_price', '18');
INSERT INTO `piano_postmeta` VALUES (140, 13, '_sku', 'woo-beanie');
INSERT INTO `piano_postmeta` VALUES (141, 13, '_regular_price', '20');
INSERT INTO `piano_postmeta` VALUES (142, 13, '_sale_price', '18');
INSERT INTO `piano_postmeta` VALUES (143, 13, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (144, 13, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (145, 13, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (146, 13, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (147, 13, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (148, 13, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (149, 13, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (150, 13, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (151, 13, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (152, 13, '_length', '');
INSERT INTO `piano_postmeta` VALUES (153, 13, '_width', '');
INSERT INTO `piano_postmeta` VALUES (154, 13, '_height', '');
INSERT INTO `piano_postmeta` VALUES (155, 13, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (156, 13, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (157, 13, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (158, 13, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (159, 13, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (160, 13, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (161, 13, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (162, 13, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (163, 13, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (164, 13, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (165, 13, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (166, 13, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (167, 13, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (168, 13, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (169, 13, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (170, 13, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (171, 13, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (172, 13, '_price', '18');
INSERT INTO `piano_postmeta` VALUES (174, 14, '_sku', 'woo-belt');
INSERT INTO `piano_postmeta` VALUES (175, 14, '_regular_price', '65');
INSERT INTO `piano_postmeta` VALUES (176, 14, '_sale_price', '55');
INSERT INTO `piano_postmeta` VALUES (177, 14, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (178, 14, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (179, 14, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (180, 14, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (181, 14, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (182, 14, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (183, 14, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (184, 14, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (185, 14, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (186, 14, '_length', '');
INSERT INTO `piano_postmeta` VALUES (187, 14, '_width', '');
INSERT INTO `piano_postmeta` VALUES (188, 14, '_height', '');
INSERT INTO `piano_postmeta` VALUES (189, 14, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (190, 14, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (191, 14, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (192, 14, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (193, 14, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (194, 14, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (195, 14, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (196, 14, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (197, 14, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (198, 14, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (199, 14, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (200, 14, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (201, 14, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (202, 14, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (203, 14, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (204, 14, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (205, 14, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (206, 14, '_price', '55');
INSERT INTO `piano_postmeta` VALUES (208, 15, '_sku', 'woo-cap');
INSERT INTO `piano_postmeta` VALUES (209, 15, '_regular_price', '18');
INSERT INTO `piano_postmeta` VALUES (210, 15, '_sale_price', '16');
INSERT INTO `piano_postmeta` VALUES (211, 15, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (212, 15, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (213, 15, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (214, 15, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (215, 15, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (216, 15, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (217, 15, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (218, 15, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (219, 15, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (220, 15, '_length', '');
INSERT INTO `piano_postmeta` VALUES (221, 15, '_width', '');
INSERT INTO `piano_postmeta` VALUES (222, 15, '_height', '');
INSERT INTO `piano_postmeta` VALUES (223, 15, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (224, 15, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (225, 15, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (226, 15, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (227, 15, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (228, 15, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (229, 15, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (230, 15, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (231, 15, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (232, 15, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (233, 15, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (234, 15, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (235, 15, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (236, 15, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (237, 15, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (238, 15, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (239, 15, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (240, 15, '_price', '16');
INSERT INTO `piano_postmeta` VALUES (242, 16, '_sku', 'woo-sunglasses');
INSERT INTO `piano_postmeta` VALUES (243, 16, '_regular_price', '90');
INSERT INTO `piano_postmeta` VALUES (244, 16, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (245, 16, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (246, 16, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (247, 16, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (248, 16, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (249, 16, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (250, 16, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (251, 16, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (252, 16, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (253, 16, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (254, 16, '_length', '');
INSERT INTO `piano_postmeta` VALUES (255, 16, '_width', '');
INSERT INTO `piano_postmeta` VALUES (256, 16, '_height', '');
INSERT INTO `piano_postmeta` VALUES (257, 16, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (258, 16, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (259, 16, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (260, 16, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (261, 16, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (262, 16, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (263, 16, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (264, 16, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (265, 16, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (266, 16, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (267, 16, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (268, 16, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (269, 16, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (270, 16, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (271, 16, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (272, 16, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (273, 16, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (274, 16, '_price', '90');
INSERT INTO `piano_postmeta` VALUES (276, 17, '_sku', 'woo-hoodie-with-pocket');
INSERT INTO `piano_postmeta` VALUES (277, 17, '_regular_price', '45');
INSERT INTO `piano_postmeta` VALUES (278, 17, '_sale_price', '35');
INSERT INTO `piano_postmeta` VALUES (279, 17, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (280, 17, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (281, 17, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (282, 17, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (283, 17, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (284, 17, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (285, 17, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (286, 17, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (287, 17, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (288, 17, '_length', '');
INSERT INTO `piano_postmeta` VALUES (289, 17, '_width', '');
INSERT INTO `piano_postmeta` VALUES (290, 17, '_height', '');
INSERT INTO `piano_postmeta` VALUES (291, 17, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (292, 17, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (293, 17, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (294, 17, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (295, 17, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (296, 17, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (297, 17, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (298, 17, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (299, 17, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (300, 17, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (301, 17, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (302, 17, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (303, 17, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (304, 17, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (305, 17, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (306, 17, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (307, 17, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (308, 17, '_price', '35');
INSERT INTO `piano_postmeta` VALUES (310, 18, '_sku', 'woo-hoodie-with-zipper');
INSERT INTO `piano_postmeta` VALUES (311, 18, '_regular_price', '45');
INSERT INTO `piano_postmeta` VALUES (312, 18, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (313, 18, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (314, 18, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (315, 18, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (316, 18, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (317, 18, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (318, 18, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (319, 18, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (320, 18, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (321, 18, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (322, 18, '_length', '');
INSERT INTO `piano_postmeta` VALUES (323, 18, '_width', '');
INSERT INTO `piano_postmeta` VALUES (324, 18, '_height', '');
INSERT INTO `piano_postmeta` VALUES (325, 18, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (326, 18, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (327, 18, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (328, 18, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (329, 18, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (330, 18, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (331, 18, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (332, 18, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (333, 18, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (334, 18, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (335, 18, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (336, 18, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (337, 18, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (338, 18, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (339, 18, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (340, 18, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (341, 18, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (342, 18, '_price', '45');
INSERT INTO `piano_postmeta` VALUES (344, 19, '_sku', 'woo-long-sleeve-tee');
INSERT INTO `piano_postmeta` VALUES (345, 19, '_regular_price', '25');
INSERT INTO `piano_postmeta` VALUES (346, 19, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (347, 19, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (348, 19, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (349, 19, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (350, 19, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (351, 19, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (352, 19, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (353, 19, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (354, 19, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (355, 19, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (356, 19, '_length', '');
INSERT INTO `piano_postmeta` VALUES (357, 19, '_width', '');
INSERT INTO `piano_postmeta` VALUES (358, 19, '_height', '');
INSERT INTO `piano_postmeta` VALUES (359, 19, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (360, 19, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (361, 19, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (362, 19, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (363, 19, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (364, 19, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (365, 19, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (366, 19, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (367, 19, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (368, 19, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (369, 19, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (370, 19, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (371, 19, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (372, 19, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (373, 19, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (374, 19, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (375, 19, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (376, 19, '_price', '25');
INSERT INTO `piano_postmeta` VALUES (378, 20, '_sku', 'woo-polo');
INSERT INTO `piano_postmeta` VALUES (379, 20, '_regular_price', '20');
INSERT INTO `piano_postmeta` VALUES (380, 20, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (381, 20, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (382, 20, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (383, 20, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (384, 20, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (385, 20, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (386, 20, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (387, 20, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (388, 20, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (389, 20, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (390, 20, '_length', '');
INSERT INTO `piano_postmeta` VALUES (391, 20, '_width', '');
INSERT INTO `piano_postmeta` VALUES (392, 20, '_height', '');
INSERT INTO `piano_postmeta` VALUES (393, 20, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (394, 20, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (395, 20, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (396, 20, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (397, 20, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (398, 20, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (399, 20, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (400, 20, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (401, 20, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (402, 20, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (403, 20, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (404, 20, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (405, 20, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (406, 20, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (407, 20, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (408, 20, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (409, 20, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (410, 20, '_price', '20');
INSERT INTO `piano_postmeta` VALUES (412, 21, '_sku', 'woo-album');
INSERT INTO `piano_postmeta` VALUES (413, 21, '_regular_price', '15');
INSERT INTO `piano_postmeta` VALUES (414, 21, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (415, 21, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (416, 21, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (417, 21, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (418, 21, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (419, 21, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (420, 21, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (421, 21, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (422, 21, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (423, 21, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (424, 21, '_length', '');
INSERT INTO `piano_postmeta` VALUES (425, 21, '_width', '');
INSERT INTO `piano_postmeta` VALUES (426, 21, '_height', '');
INSERT INTO `piano_postmeta` VALUES (427, 21, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (428, 21, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (429, 21, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (430, 21, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (431, 21, '_virtual', 'yes');
INSERT INTO `piano_postmeta` VALUES (432, 21, '_downloadable', 'yes');
INSERT INTO `piano_postmeta` VALUES (433, 21, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (434, 21, '_download_limit', '1');
INSERT INTO `piano_postmeta` VALUES (435, 21, '_download_expiry', '1');
INSERT INTO `piano_postmeta` VALUES (436, 21, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (437, 21, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (438, 21, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (439, 21, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (440, 21, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (441, 21, '_downloadable_files', 'a:2:{s:36:\"03a03c92-ad54-41b2-97cc-4260d9215672\";a:3:{s:2:\"id\";s:36:\"03a03c92-ad54-41b2-97cc-4260d9215672\";s:4:\"name\";s:8:\"Single 1\";s:4:\"file\";s:85:\"https://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2017/08/single.jpg\";}s:36:\"3b9f75c9-8bc0-4c01-8594-1ed032b68ca4\";a:3:{s:2:\"id\";s:36:\"3b9f75c9-8bc0-4c01-8594-1ed032b68ca4\";s:4:\"name\";s:8:\"Single 2\";s:4:\"file\";s:84:\"https://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2017/08/album.jpg\";}}');
INSERT INTO `piano_postmeta` VALUES (442, 21, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (443, 21, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (444, 21, '_price', '15');
INSERT INTO `piano_postmeta` VALUES (446, 22, '_sku', 'woo-single');
INSERT INTO `piano_postmeta` VALUES (447, 22, '_regular_price', '3');
INSERT INTO `piano_postmeta` VALUES (448, 22, '_sale_price', '2');
INSERT INTO `piano_postmeta` VALUES (449, 22, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (450, 22, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (451, 22, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (452, 22, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (453, 22, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (454, 22, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (455, 22, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (456, 22, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (457, 22, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (458, 22, '_length', '');
INSERT INTO `piano_postmeta` VALUES (459, 22, '_width', '');
INSERT INTO `piano_postmeta` VALUES (460, 22, '_height', '');
INSERT INTO `piano_postmeta` VALUES (461, 22, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (462, 22, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (463, 22, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (464, 22, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (465, 22, '_virtual', 'yes');
INSERT INTO `piano_postmeta` VALUES (466, 22, '_downloadable', 'yes');
INSERT INTO `piano_postmeta` VALUES (467, 22, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (468, 22, '_download_limit', '1');
INSERT INTO `piano_postmeta` VALUES (469, 22, '_download_expiry', '1');
INSERT INTO `piano_postmeta` VALUES (470, 22, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (471, 22, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (472, 22, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (473, 22, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (474, 22, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (475, 22, '_downloadable_files', 'a:1:{s:36:\"210f4756-ef60-4710-99dd-502a9966b86f\";a:3:{s:2:\"id\";s:36:\"210f4756-ef60-4710-99dd-502a9966b86f\";s:4:\"name\";s:6:\"Single\";s:4:\"file\";s:85:\"https://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2017/08/single.jpg\";}}');
INSERT INTO `piano_postmeta` VALUES (476, 22, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (477, 22, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (478, 22, '_price', '2');
INSERT INTO `piano_postmeta` VALUES (480, 23, '_sku', 'woo-vneck-tee-red');
INSERT INTO `piano_postmeta` VALUES (481, 23, '_regular_price', '20');
INSERT INTO `piano_postmeta` VALUES (482, 23, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (483, 23, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (484, 23, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (485, 23, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (486, 23, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (487, 23, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (488, 23, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (489, 23, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (490, 23, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (491, 23, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (492, 23, '_length', '');
INSERT INTO `piano_postmeta` VALUES (493, 23, '_width', '');
INSERT INTO `piano_postmeta` VALUES (494, 23, '_height', '');
INSERT INTO `piano_postmeta` VALUES (495, 23, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (496, 23, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (497, 23, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (498, 23, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (499, 23, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (500, 23, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (501, 23, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (502, 23, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (503, 23, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (504, 23, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (505, 23, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (506, 23, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (507, 23, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (508, 23, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (509, 23, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (510, 23, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (511, 23, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (512, 23, '_price', '20');
INSERT INTO `piano_postmeta` VALUES (514, 24, '_sku', 'woo-vneck-tee-green');
INSERT INTO `piano_postmeta` VALUES (515, 24, '_regular_price', '20');
INSERT INTO `piano_postmeta` VALUES (516, 24, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (517, 24, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (518, 24, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (519, 24, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (520, 24, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (521, 24, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (522, 24, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (523, 24, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (524, 24, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (525, 24, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (526, 24, '_length', '');
INSERT INTO `piano_postmeta` VALUES (527, 24, '_width', '');
INSERT INTO `piano_postmeta` VALUES (528, 24, '_height', '');
INSERT INTO `piano_postmeta` VALUES (529, 24, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (530, 24, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (531, 24, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (532, 24, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (533, 24, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (534, 24, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (535, 24, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (536, 24, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (537, 24, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (538, 24, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (539, 24, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (540, 24, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (541, 24, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (542, 24, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (543, 24, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (544, 24, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (545, 24, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (546, 24, '_price', '20');
INSERT INTO `piano_postmeta` VALUES (548, 25, '_sku', 'woo-vneck-tee-blue');
INSERT INTO `piano_postmeta` VALUES (549, 25, '_regular_price', '15');
INSERT INTO `piano_postmeta` VALUES (550, 25, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (551, 25, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (552, 25, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (553, 25, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (554, 25, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (555, 25, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (556, 25, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (557, 25, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (558, 25, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (559, 25, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (560, 25, '_length', '');
INSERT INTO `piano_postmeta` VALUES (561, 25, '_width', '');
INSERT INTO `piano_postmeta` VALUES (562, 25, '_height', '');
INSERT INTO `piano_postmeta` VALUES (563, 25, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (564, 25, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (565, 25, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (566, 25, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (567, 25, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (568, 25, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (569, 25, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (570, 25, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (571, 25, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (572, 25, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (573, 25, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (574, 25, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (575, 25, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (576, 25, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (577, 25, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (578, 25, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (579, 25, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (580, 25, '_price', '15');
INSERT INTO `piano_postmeta` VALUES (582, 26, '_sku', 'woo-hoodie-red');
INSERT INTO `piano_postmeta` VALUES (583, 26, '_regular_price', '45');
INSERT INTO `piano_postmeta` VALUES (584, 26, '_sale_price', '42');
INSERT INTO `piano_postmeta` VALUES (585, 26, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (586, 26, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (587, 26, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (588, 26, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (589, 26, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (590, 26, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (591, 26, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (592, 26, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (593, 26, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (594, 26, '_length', '');
INSERT INTO `piano_postmeta` VALUES (595, 26, '_width', '');
INSERT INTO `piano_postmeta` VALUES (596, 26, '_height', '');
INSERT INTO `piano_postmeta` VALUES (597, 26, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (598, 26, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (599, 26, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (600, 26, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (601, 26, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (602, 26, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (603, 26, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (604, 26, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (605, 26, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (606, 26, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (607, 26, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (608, 26, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (609, 26, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (610, 26, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (611, 26, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (612, 26, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (613, 26, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (614, 26, '_price', '42');
INSERT INTO `piano_postmeta` VALUES (616, 27, '_sku', 'woo-hoodie-green');
INSERT INTO `piano_postmeta` VALUES (617, 27, '_regular_price', '45');
INSERT INTO `piano_postmeta` VALUES (618, 27, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (619, 27, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (620, 27, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (621, 27, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (622, 27, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (623, 27, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (624, 27, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (625, 27, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (626, 27, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (627, 27, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (628, 27, '_length', '');
INSERT INTO `piano_postmeta` VALUES (629, 27, '_width', '');
INSERT INTO `piano_postmeta` VALUES (630, 27, '_height', '');
INSERT INTO `piano_postmeta` VALUES (631, 27, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (632, 27, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (633, 27, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (634, 27, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (635, 27, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (636, 27, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (637, 27, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (638, 27, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (639, 27, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (640, 27, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (641, 27, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (642, 27, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (643, 27, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (644, 27, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (645, 27, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (646, 27, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (647, 27, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (648, 27, '_price', '45');
INSERT INTO `piano_postmeta` VALUES (650, 28, '_sku', 'woo-hoodie-blue');
INSERT INTO `piano_postmeta` VALUES (651, 28, '_regular_price', '45');
INSERT INTO `piano_postmeta` VALUES (652, 28, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (653, 28, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (654, 28, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (655, 28, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (656, 28, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (657, 28, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (658, 28, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (659, 28, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (660, 28, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (661, 28, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (662, 28, '_length', '');
INSERT INTO `piano_postmeta` VALUES (663, 28, '_width', '');
INSERT INTO `piano_postmeta` VALUES (664, 28, '_height', '');
INSERT INTO `piano_postmeta` VALUES (665, 28, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (666, 28, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (667, 28, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (668, 28, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (669, 28, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (670, 28, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (671, 28, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (672, 28, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (673, 28, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (674, 28, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (675, 28, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (676, 28, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (677, 28, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (678, 28, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (679, 28, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (680, 28, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (681, 28, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (682, 28, '_price', '45');
INSERT INTO `piano_postmeta` VALUES (684, 29, '_sku', 'Woo-tshirt-logo');
INSERT INTO `piano_postmeta` VALUES (685, 29, '_regular_price', '18');
INSERT INTO `piano_postmeta` VALUES (686, 29, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (687, 29, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (688, 29, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (689, 29, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (690, 29, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (691, 29, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (692, 29, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (693, 29, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (694, 29, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (695, 29, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (696, 29, '_length', '');
INSERT INTO `piano_postmeta` VALUES (697, 29, '_width', '');
INSERT INTO `piano_postmeta` VALUES (698, 29, '_height', '');
INSERT INTO `piano_postmeta` VALUES (699, 29, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (700, 29, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (701, 29, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (702, 29, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (703, 29, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (704, 29, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (705, 29, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (706, 29, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (707, 29, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (708, 29, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (709, 29, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (710, 29, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (711, 29, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (712, 29, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (713, 29, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (714, 29, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (715, 29, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (716, 29, '_price', '18');
INSERT INTO `piano_postmeta` VALUES (718, 30, '_sku', 'Woo-beanie-logo');
INSERT INTO `piano_postmeta` VALUES (719, 30, '_regular_price', '20');
INSERT INTO `piano_postmeta` VALUES (720, 30, '_sale_price', '18');
INSERT INTO `piano_postmeta` VALUES (721, 30, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (722, 30, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (723, 30, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (724, 30, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (725, 30, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (726, 30, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (727, 30, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (728, 30, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (729, 30, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (730, 30, '_length', '');
INSERT INTO `piano_postmeta` VALUES (731, 30, '_width', '');
INSERT INTO `piano_postmeta` VALUES (732, 30, '_height', '');
INSERT INTO `piano_postmeta` VALUES (733, 30, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (734, 30, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (735, 30, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (736, 30, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (737, 30, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (738, 30, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (739, 30, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (740, 30, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (741, 30, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (742, 30, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (743, 30, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (744, 30, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (745, 30, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (746, 30, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (747, 30, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (748, 30, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}');
INSERT INTO `piano_postmeta` VALUES (749, 30, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (750, 30, '_price', '18');
INSERT INTO `piano_postmeta` VALUES (752, 31, '_sku', 'logo-collection');
INSERT INTO `piano_postmeta` VALUES (753, 31, '_regular_price', '');
INSERT INTO `piano_postmeta` VALUES (754, 31, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (755, 31, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (756, 31, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (757, 31, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (758, 31, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (759, 31, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (760, 31, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (761, 31, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (762, 31, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (763, 31, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (764, 31, '_length', '');
INSERT INTO `piano_postmeta` VALUES (765, 31, '_width', '');
INSERT INTO `piano_postmeta` VALUES (766, 31, '_height', '');
INSERT INTO `piano_postmeta` VALUES (767, 31, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (768, 31, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (769, 31, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (770, 31, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (771, 31, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (772, 31, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (773, 31, '_product_image_gallery', '53,52,40');
INSERT INTO `piano_postmeta` VALUES (774, 31, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (775, 31, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (776, 31, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (777, 31, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (778, 31, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (779, 31, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (780, 31, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (781, 31, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (782, 31, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (783, 31, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (786, 32, '_sku', 'wp-pennant');
INSERT INTO `piano_postmeta` VALUES (787, 32, '_regular_price', '11.05');
INSERT INTO `piano_postmeta` VALUES (788, 32, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (789, 32, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (790, 32, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (791, 32, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (792, 32, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (793, 32, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (794, 32, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (795, 32, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (796, 32, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (797, 32, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (798, 32, '_length', '');
INSERT INTO `piano_postmeta` VALUES (799, 32, '_width', '');
INSERT INTO `piano_postmeta` VALUES (800, 32, '_height', '');
INSERT INTO `piano_postmeta` VALUES (801, 32, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (802, 32, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (803, 32, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (804, 32, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (805, 32, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (806, 32, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (807, 32, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (808, 32, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (809, 32, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (810, 32, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (811, 32, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (812, 32, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (813, 32, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (814, 32, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (815, 32, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (816, 32, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (817, 32, '_product_version', '3.4.0');
INSERT INTO `piano_postmeta` VALUES (818, 32, '_price', '11.05');
INSERT INTO `piano_postmeta` VALUES (820, 33, '_sku', 'woo-hoodie-blue-logo');
INSERT INTO `piano_postmeta` VALUES (821, 33, '_regular_price', '45');
INSERT INTO `piano_postmeta` VALUES (822, 33, '_sale_price', '');
INSERT INTO `piano_postmeta` VALUES (823, 33, '_sale_price_dates_from', '');
INSERT INTO `piano_postmeta` VALUES (824, 33, '_sale_price_dates_to', '');
INSERT INTO `piano_postmeta` VALUES (825, 33, 'total_sales', '0');
INSERT INTO `piano_postmeta` VALUES (826, 33, '_tax_status', 'taxable');
INSERT INTO `piano_postmeta` VALUES (827, 33, '_tax_class', '');
INSERT INTO `piano_postmeta` VALUES (828, 33, '_manage_stock', 'no');
INSERT INTO `piano_postmeta` VALUES (829, 33, '_backorders', 'no');
INSERT INTO `piano_postmeta` VALUES (830, 33, '_sold_individually', 'no');
INSERT INTO `piano_postmeta` VALUES (831, 33, '_weight', '');
INSERT INTO `piano_postmeta` VALUES (832, 33, '_length', '');
INSERT INTO `piano_postmeta` VALUES (833, 33, '_width', '');
INSERT INTO `piano_postmeta` VALUES (834, 33, '_height', '');
INSERT INTO `piano_postmeta` VALUES (835, 33, '_upsell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (836, 33, '_crosssell_ids', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (837, 33, '_purchase_note', '');
INSERT INTO `piano_postmeta` VALUES (838, 33, '_default_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (839, 33, '_virtual', 'no');
INSERT INTO `piano_postmeta` VALUES (840, 33, '_downloadable', 'no');
INSERT INTO `piano_postmeta` VALUES (841, 33, '_product_image_gallery', '');
INSERT INTO `piano_postmeta` VALUES (842, 33, '_download_limit', '0');
INSERT INTO `piano_postmeta` VALUES (843, 33, '_download_expiry', '0');
INSERT INTO `piano_postmeta` VALUES (844, 33, '_stock', NULL);
INSERT INTO `piano_postmeta` VALUES (845, 33, '_stock_status', 'instock');
INSERT INTO `piano_postmeta` VALUES (846, 33, '_wc_average_rating', '0');
INSERT INTO `piano_postmeta` VALUES (847, 33, '_wc_rating_count', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (848, 33, '_wc_review_count', '0');
INSERT INTO `piano_postmeta` VALUES (849, 33, '_downloadable_files', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (850, 33, '_product_attributes', 'a:0:{}');
INSERT INTO `piano_postmeta` VALUES (851, 33, '_product_version', '3.3.5');
INSERT INTO `piano_postmeta` VALUES (852, 33, '_price', '45');
INSERT INTO `piano_postmeta` VALUES (854, 34, '_wp_attached_file', '2018/05/vneck-tee-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (855, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:25:\"2018/05/vneck-tee-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"vneck-tee-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"vneck-tee-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (856, 34, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vneck-tee-2.jpg');
INSERT INTO `piano_postmeta` VALUES (857, 35, '_wp_attached_file', '2018/05/vnech-tee-green-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (858, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:31:\"2018/05/vnech-tee-green-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"vnech-tee-green-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (859, 35, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-green-1.jpg');
INSERT INTO `piano_postmeta` VALUES (860, 36, '_wp_attached_file', '2018/05/vnech-tee-blue-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (861, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:30:\"2018/05/vnech-tee-blue-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"vnech-tee-blue-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (862, 36, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-blue-1.jpg');
INSERT INTO `piano_postmeta` VALUES (863, 9, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (864, 9, '_wp_old_slug', 'import-placeholder-for-44');
INSERT INTO `piano_postmeta` VALUES (865, 9, '_thumbnail_id', '34');
INSERT INTO `piano_postmeta` VALUES (866, 37, '_wp_attached_file', '2018/05/hoodie-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (867, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:22:\"2018/05/hoodie-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"hoodie-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"hoodie-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (868, 37, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-2.jpg');
INSERT INTO `piano_postmeta` VALUES (869, 38, '_wp_attached_file', '2018/05/hoodie-blue-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (870, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:27:\"2018/05/hoodie-blue-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"hoodie-blue-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"hoodie-blue-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (871, 38, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-blue-1.jpg');
INSERT INTO `piano_postmeta` VALUES (872, 39, '_wp_attached_file', '2018/05/hoodie-green-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (873, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:28:\"2018/05/hoodie-green-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"hoodie-green-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:28:\"hoodie-green-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (874, 39, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-green-1.jpg');
INSERT INTO `piano_postmeta` VALUES (875, 40, '_wp_attached_file', '2018/05/hoodie-with-logo-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (876, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:32:\"2018/05/hoodie-with-logo-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-logo-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (877, 40, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-logo-2.jpg');
INSERT INTO `piano_postmeta` VALUES (878, 10, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (879, 10, '_wp_old_slug', 'import-placeholder-for-45');
INSERT INTO `piano_postmeta` VALUES (880, 10, '_thumbnail_id', '37');
INSERT INTO `piano_postmeta` VALUES (881, 11, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (882, 11, '_wp_old_slug', 'import-placeholder-for-46');
INSERT INTO `piano_postmeta` VALUES (883, 11, '_thumbnail_id', '40');
INSERT INTO `piano_postmeta` VALUES (884, 41, '_wp_attached_file', '2018/05/tshirt-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (885, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:22:\"2018/05/tshirt-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"tshirt-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"tshirt-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (886, 41, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/tshirt-2.jpg');
INSERT INTO `piano_postmeta` VALUES (887, 12, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (888, 12, '_wp_old_slug', 'import-placeholder-for-47');
INSERT INTO `piano_postmeta` VALUES (889, 12, '_thumbnail_id', '41');
INSERT INTO `piano_postmeta` VALUES (890, 42, '_wp_attached_file', '2018/05/beanie-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (891, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:22:\"2018/05/beanie-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"beanie-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"beanie-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (892, 42, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/beanie-2.jpg');
INSERT INTO `piano_postmeta` VALUES (893, 13, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (894, 13, '_wp_old_slug', 'import-placeholder-for-48');
INSERT INTO `piano_postmeta` VALUES (895, 13, '_thumbnail_id', '42');
INSERT INTO `piano_postmeta` VALUES (896, 43, '_wp_attached_file', '2018/05/belt-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (897, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:20:\"2018/05/belt-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"belt-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"belt-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"belt-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"belt-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"belt-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"belt-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"belt-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"belt-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"belt-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (898, 43, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/belt-2.jpg');
INSERT INTO `piano_postmeta` VALUES (899, 14, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (900, 14, '_wp_old_slug', 'import-placeholder-for-58');
INSERT INTO `piano_postmeta` VALUES (901, 14, '_thumbnail_id', '43');
INSERT INTO `piano_postmeta` VALUES (902, 44, '_wp_attached_file', '2018/05/cap-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (903, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:19:\"2018/05/cap-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"cap-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"cap-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"cap-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"cap-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"cap-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"cap-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"cap-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:19:\"cap-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"cap-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (904, 44, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/cap-2.jpg');
INSERT INTO `piano_postmeta` VALUES (905, 15, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (906, 15, '_wp_old_slug', 'import-placeholder-for-60');
INSERT INTO `piano_postmeta` VALUES (907, 15, '_thumbnail_id', '44');
INSERT INTO `piano_postmeta` VALUES (908, 45, '_wp_attached_file', '2018/05/sunglasses-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (909, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:26:\"2018/05/sunglasses-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"sunglasses-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"sunglasses-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (910, 45, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/sunglasses-2.jpg');
INSERT INTO `piano_postmeta` VALUES (911, 16, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (912, 16, '_wp_old_slug', 'import-placeholder-for-62');
INSERT INTO `piano_postmeta` VALUES (913, 16, '_thumbnail_id', '45');
INSERT INTO `piano_postmeta` VALUES (914, 46, '_wp_attached_file', '2018/05/hoodie-with-pocket-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (915, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:34:\"2018/05/hoodie-with-pocket-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"hoodie-with-pocket-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (916, 46, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-pocket-2.jpg');
INSERT INTO `piano_postmeta` VALUES (917, 17, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (918, 17, '_wp_old_slug', 'import-placeholder-for-64');
INSERT INTO `piano_postmeta` VALUES (919, 17, '_thumbnail_id', '46');
INSERT INTO `piano_postmeta` VALUES (920, 47, '_wp_attached_file', '2018/05/hoodie-with-zipper-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (921, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:34:\"2018/05/hoodie-with-zipper-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"hoodie-with-zipper-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (922, 47, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-zipper-2.jpg');
INSERT INTO `piano_postmeta` VALUES (923, 18, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (924, 18, '_wp_old_slug', 'import-placeholder-for-66');
INSERT INTO `piano_postmeta` VALUES (925, 18, '_thumbnail_id', '47');
INSERT INTO `piano_postmeta` VALUES (926, 48, '_wp_attached_file', '2018/05/long-sleeve-tee-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (927, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:31:\"2018/05/long-sleeve-tee-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"long-sleeve-tee-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (928, 48, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/long-sleeve-tee-2.jpg');
INSERT INTO `piano_postmeta` VALUES (929, 19, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (930, 19, '_wp_old_slug', 'import-placeholder-for-68');
INSERT INTO `piano_postmeta` VALUES (931, 19, '_thumbnail_id', '48');
INSERT INTO `piano_postmeta` VALUES (932, 49, '_wp_attached_file', '2018/05/polo-2-1.jpg');
INSERT INTO `piano_postmeta` VALUES (933, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:20:\"2018/05/polo-2-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"polo-2-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"polo-2-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"polo-2-1-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"polo-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"polo-2-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"polo-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"polo-2-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"polo-2-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"polo-2-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (934, 49, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/polo-2.jpg');
INSERT INTO `piano_postmeta` VALUES (935, 20, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (936, 20, '_wp_old_slug', 'import-placeholder-for-70');
INSERT INTO `piano_postmeta` VALUES (937, 20, '_thumbnail_id', '49');
INSERT INTO `piano_postmeta` VALUES (938, 50, '_wp_attached_file', '2018/05/album-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (939, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:21:\"2018/05/album-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"album-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"album-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"album-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"album-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"album-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"album-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"album-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"album-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"album-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (940, 50, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/album-1.jpg');
INSERT INTO `piano_postmeta` VALUES (941, 21, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (942, 21, '_wp_old_slug', 'import-placeholder-for-73');
INSERT INTO `piano_postmeta` VALUES (943, 21, '_thumbnail_id', '50');
INSERT INTO `piano_postmeta` VALUES (944, 51, '_wp_attached_file', '2018/05/single-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (945, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:22:\"2018/05/single-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"single-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"single-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"single-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"single-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"single-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"single-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"single-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"single-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"single-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (946, 51, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/single-1.jpg');
INSERT INTO `piano_postmeta` VALUES (947, 22, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (948, 22, '_wp_old_slug', 'import-placeholder-for-75');
INSERT INTO `piano_postmeta` VALUES (949, 22, '_thumbnail_id', '51');
INSERT INTO `piano_postmeta` VALUES (950, 23, '_wpcom_is_markdown', '');
INSERT INTO `piano_postmeta` VALUES (951, 23, '_wp_old_slug', 'import-placeholder-for-76');
INSERT INTO `piano_postmeta` VALUES (952, 23, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.');
INSERT INTO `piano_postmeta` VALUES (953, 23, '_thumbnail_id', '34');
INSERT INTO `piano_postmeta` VALUES (954, 23, 'attribute_pa_color', 'red');
INSERT INTO `piano_postmeta` VALUES (955, 23, 'attribute_pa_size', '');
INSERT INTO `piano_postmeta` VALUES (956, 24, '_wpcom_is_markdown', '');
INSERT INTO `piano_postmeta` VALUES (957, 24, '_wp_old_slug', 'import-placeholder-for-77');
INSERT INTO `piano_postmeta` VALUES (958, 24, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.');
INSERT INTO `piano_postmeta` VALUES (959, 24, '_thumbnail_id', '35');
INSERT INTO `piano_postmeta` VALUES (960, 24, 'attribute_pa_color', 'green');
INSERT INTO `piano_postmeta` VALUES (961, 24, 'attribute_pa_size', '');
INSERT INTO `piano_postmeta` VALUES (962, 25, '_wpcom_is_markdown', '');
INSERT INTO `piano_postmeta` VALUES (963, 25, '_wp_old_slug', 'import-placeholder-for-78');
INSERT INTO `piano_postmeta` VALUES (964, 25, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.');
INSERT INTO `piano_postmeta` VALUES (965, 25, '_thumbnail_id', '36');
INSERT INTO `piano_postmeta` VALUES (966, 25, 'attribute_pa_color', 'blue');
INSERT INTO `piano_postmeta` VALUES (967, 25, 'attribute_pa_size', '');
INSERT INTO `piano_postmeta` VALUES (968, 26, '_wpcom_is_markdown', '');
INSERT INTO `piano_postmeta` VALUES (969, 26, '_wp_old_slug', 'import-placeholder-for-79');
INSERT INTO `piano_postmeta` VALUES (970, 26, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.');
INSERT INTO `piano_postmeta` VALUES (971, 26, '_thumbnail_id', '37');
INSERT INTO `piano_postmeta` VALUES (972, 26, 'attribute_pa_color', 'red');
INSERT INTO `piano_postmeta` VALUES (973, 26, 'attribute_logo', 'No');
INSERT INTO `piano_postmeta` VALUES (974, 27, '_wpcom_is_markdown', '');
INSERT INTO `piano_postmeta` VALUES (975, 27, '_wp_old_slug', 'import-placeholder-for-80');
INSERT INTO `piano_postmeta` VALUES (976, 27, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.');
INSERT INTO `piano_postmeta` VALUES (977, 27, '_thumbnail_id', '39');
INSERT INTO `piano_postmeta` VALUES (978, 27, 'attribute_pa_color', 'green');
INSERT INTO `piano_postmeta` VALUES (979, 27, 'attribute_logo', 'No');
INSERT INTO `piano_postmeta` VALUES (980, 28, '_wpcom_is_markdown', '');
INSERT INTO `piano_postmeta` VALUES (981, 28, '_wp_old_slug', 'import-placeholder-for-81');
INSERT INTO `piano_postmeta` VALUES (982, 28, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.');
INSERT INTO `piano_postmeta` VALUES (983, 28, '_thumbnail_id', '38');
INSERT INTO `piano_postmeta` VALUES (984, 28, 'attribute_pa_color', 'blue');
INSERT INTO `piano_postmeta` VALUES (985, 28, 'attribute_logo', 'No');
INSERT INTO `piano_postmeta` VALUES (986, 52, '_wp_attached_file', '2018/05/t-shirt-with-logo-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (987, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:33:\"2018/05/t-shirt-with-logo-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"t-shirt-with-logo-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (988, 52, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/t-shirt-with-logo-1.jpg');
INSERT INTO `piano_postmeta` VALUES (989, 29, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (990, 29, '_wp_old_slug', 'import-placeholder-for-83');
INSERT INTO `piano_postmeta` VALUES (991, 29, '_thumbnail_id', '52');
INSERT INTO `piano_postmeta` VALUES (992, 53, '_wp_attached_file', '2018/05/beanie-with-logo-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (993, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:32:\"2018/05/beanie-with-logo-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"beanie-with-logo-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (994, 53, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/beanie-with-logo-1.jpg');
INSERT INTO `piano_postmeta` VALUES (995, 30, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (996, 30, '_wp_old_slug', 'import-placeholder-for-85');
INSERT INTO `piano_postmeta` VALUES (997, 30, '_thumbnail_id', '53');
INSERT INTO `piano_postmeta` VALUES (998, 54, '_wp_attached_file', '2018/05/logo-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (999, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:799;s:4:\"file\";s:20:\"2018/05/logo-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"logo-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"logo-1-1-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"logo-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"logo-1-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"logo-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"logo-1-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1000, 54, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/logo-1.jpg');
INSERT INTO `piano_postmeta` VALUES (1001, 31, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (1002, 31, '_wp_old_slug', 'import-placeholder-for-87');
INSERT INTO `piano_postmeta` VALUES (1003, 31, '_children', 'a:3:{i:0;i:11;i:1;i:12;i:2;i:13;}');
INSERT INTO `piano_postmeta` VALUES (1004, 31, '_thumbnail_id', '54');
INSERT INTO `piano_postmeta` VALUES (1005, 31, '_price', '');
INSERT INTO `piano_postmeta` VALUES (1006, 31, '_price', '');
INSERT INTO `piano_postmeta` VALUES (1007, 55, '_wp_attached_file', '2018/05/pennant-1-1.jpg');
INSERT INTO `piano_postmeta` VALUES (1008, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:23:\"2018/05/pennant-1-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"pennant-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"pennant-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1009, 55, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/pennant-1.jpg');
INSERT INTO `piano_postmeta` VALUES (1010, 32, '_wpcom_is_markdown', '1');
INSERT INTO `piano_postmeta` VALUES (1011, 32, '_wp_old_slug', 'import-placeholder-for-89');
INSERT INTO `piano_postmeta` VALUES (1012, 32, '_thumbnail_id', '55');
INSERT INTO `piano_postmeta` VALUES (1013, 32, '_product_url', 'https://mercantile.wordpress.org/product/wordpress-pennant/');
INSERT INTO `piano_postmeta` VALUES (1014, 32, '_button_text', 'Buy on the WordPress swag store!');
INSERT INTO `piano_postmeta` VALUES (1015, 33, '_wpcom_is_markdown', '');
INSERT INTO `piano_postmeta` VALUES (1016, 33, '_wp_old_slug', 'import-placeholder-for-90');
INSERT INTO `piano_postmeta` VALUES (1017, 33, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.');
INSERT INTO `piano_postmeta` VALUES (1018, 33, '_thumbnail_id', '40');
INSERT INTO `piano_postmeta` VALUES (1019, 33, 'attribute_pa_color', 'blue');
INSERT INTO `piano_postmeta` VALUES (1020, 33, 'attribute_logo', 'Yes');
INSERT INTO `piano_postmeta` VALUES (1021, 9, '_price', '15');
INSERT INTO `piano_postmeta` VALUES (1022, 9, '_price', '20');
INSERT INTO `piano_postmeta` VALUES (1023, 10, '_price', '42');
INSERT INTO `piano_postmeta` VALUES (1024, 10, '_price', '45');
INSERT INTO `piano_postmeta` VALUES (1025, 56, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1026, 56, '_edit_lock', '1526889680:1');
INSERT INTO `piano_postmeta` VALUES (1027, 56, '_wp_page_template', 'template-homepage.php');
INSERT INTO `piano_postmeta` VALUES (1028, 58, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1029, 58, '_edit_lock', '1526441947:1');
INSERT INTO `piano_postmeta` VALUES (1030, 58, '_wp_page_template', 'default');
INSERT INTO `piano_postmeta` VALUES (1031, 61, '_wp_attached_file', '2018/05/banner-center@3x.png');
INSERT INTO `piano_postmeta` VALUES (1032, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1620;s:6:\"height\";i:930;s:4:\"file\";s:28:\"2018/05/banner-center@3x.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-300x172.png\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-768x441.png\";s:5:\"width\";i:768;s:6:\"height\";i:441;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"banner-center@3x-1024x588.png\";s:5:\"width\";i:1024;s:6:\"height\";i:588;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"banner-center@3x-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-416x239.png\";s:5:\"width\";i:416;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-416x239.png\";s:5:\"width\";i:416;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-center@3x-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1033, 62, '_wp_attached_file', '2018/05/banner-right@3x.png');
INSERT INTO `piano_postmeta` VALUES (1034, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:900;s:6:\"height\";i:930;s:4:\"file\";s:27:\"2018/05/banner-right@3x.png\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-290x300.png\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-768x794.png\";s:5:\"width\";i:768;s:6:\"height\";i:794;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"banner-right@3x-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-416x430.png\";s:5:\"width\";i:416;s:6:\"height\";i:430;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-416x430.png\";s:5:\"width\";i:416;s:6:\"height\";i:430;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"banner-right@3x-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1035, 60, '_wpdh_image_ids', '64,65');
INSERT INTO `piano_postmeta` VALUES (1036, 60, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1037, 60, '_content_slider_settings', 'a:4:{s:12:\"slide_height\";s:5:\"400px\";s:13:\"content_width\";s:5:\"850px\";s:17:\"content_animation\";s:0:\"\";s:13:\"slide_padding\";a:4:{s:3:\"top\";s:4:\"1rem\";s:5:\"right\";s:4:\"3rem\";s:6:\"bottom\";s:4:\"1rem\";s:4:\"left\";s:4:\"3rem\";}}');
INSERT INTO `piano_postmeta` VALUES (1038, 60, '_nav_button', 'on');
INSERT INTO `piano_postmeta` VALUES (1039, 60, '_slide_by', '1');
INSERT INTO `piano_postmeta` VALUES (1040, 60, '_arrow_position', 'outside');
INSERT INTO `piano_postmeta` VALUES (1041, 60, '_arrow_size', '48');
INSERT INTO `piano_postmeta` VALUES (1042, 60, '_dot_nav', 'off');
INSERT INTO `piano_postmeta` VALUES (1043, 60, '_bullet_position', 'center');
INSERT INTO `piano_postmeta` VALUES (1044, 60, '_bullet_size', '10');
INSERT INTO `piano_postmeta` VALUES (1045, 60, '_bullet_shape', 'square');
INSERT INTO `piano_postmeta` VALUES (1046, 60, '_nav_color', '#f1f1f1');
INSERT INTO `piano_postmeta` VALUES (1047, 60, '_nav_active_color', '#00d1b2');
INSERT INTO `piano_postmeta` VALUES (1048, 60, '_autoplay', 'on');
INSERT INTO `piano_postmeta` VALUES (1049, 60, '_autoplay_pause', 'on');
INSERT INTO `piano_postmeta` VALUES (1050, 60, '_autoplay_timeout', '5000');
INSERT INTO `piano_postmeta` VALUES (1051, 60, '_autoplay_speed', '500');
INSERT INTO `piano_postmeta` VALUES (1052, 60, '_items', '4');
INSERT INTO `piano_postmeta` VALUES (1053, 60, '_items_desktop', '4');
INSERT INTO `piano_postmeta` VALUES (1054, 60, '_items_small_desktop', '4');
INSERT INTO `piano_postmeta` VALUES (1055, 60, '_items_portrait_tablet', '3');
INSERT INTO `piano_postmeta` VALUES (1056, 60, '_items_small_portrait_tablet', '2');
INSERT INTO `piano_postmeta` VALUES (1057, 60, '_items_portrait_mobile', '1');
INSERT INTO `piano_postmeta` VALUES (1058, 60, '_slide_type', 'image-carousel');
INSERT INTO `piano_postmeta` VALUES (1059, 60, '_post_query_type', 'latest_posts');
INSERT INTO `piano_postmeta` VALUES (1060, 60, '_post_date_after', '');
INSERT INTO `piano_postmeta` VALUES (1061, 60, '_post_date_before', '');
INSERT INTO `piano_postmeta` VALUES (1062, 60, '_posts_per_page', '12');
INSERT INTO `piano_postmeta` VALUES (1063, 60, '_post_order', 'DESC');
INSERT INTO `piano_postmeta` VALUES (1064, 60, '_post_orderby', 'ID');
INSERT INTO `piano_postmeta` VALUES (1065, 60, '_post_height', '450');
INSERT INTO `piano_postmeta` VALUES (1066, 60, '_product_query_type', 'query_porduct');
INSERT INTO `piano_postmeta` VALUES (1067, 60, '_product_query', 'featured');
INSERT INTO `piano_postmeta` VALUES (1068, 60, '_products_per_page', '12');
INSERT INTO `piano_postmeta` VALUES (1069, 60, '_product_title', 'on');
INSERT INTO `piano_postmeta` VALUES (1070, 60, '_product_rating', 'on');
INSERT INTO `piano_postmeta` VALUES (1071, 60, '_product_price', 'on');
INSERT INTO `piano_postmeta` VALUES (1072, 60, '_product_cart_button', 'on');
INSERT INTO `piano_postmeta` VALUES (1073, 60, '_product_onsale', 'on');
INSERT INTO `piano_postmeta` VALUES (1074, 60, '_product_wishlist', 'off');
INSERT INTO `piano_postmeta` VALUES (1075, 60, '_product_quick_view', 'on');
INSERT INTO `piano_postmeta` VALUES (1076, 60, '_product_title_color', '#323232');
INSERT INTO `piano_postmeta` VALUES (1077, 60, '_product_button_bg_color', '#00d1b2');
INSERT INTO `piano_postmeta` VALUES (1078, 60, '_product_button_text_color', '#f1f1f1');
INSERT INTO `piano_postmeta` VALUES (1079, 60, '_video_url', '');
INSERT INTO `piano_postmeta` VALUES (1080, 60, '_show_attachment_title', 'off');
INSERT INTO `piano_postmeta` VALUES (1081, 60, '_show_attachment_caption', 'off');
INSERT INTO `piano_postmeta` VALUES (1082, 60, '_image_target', '_self');
INSERT INTO `piano_postmeta` VALUES (1083, 60, '_image_lightbox', 'off');
INSERT INTO `piano_postmeta` VALUES (1084, 60, '_image_size', 'large');
INSERT INTO `piano_postmeta` VALUES (1085, 60, '_lazy_load_image', 'on');
INSERT INTO `piano_postmeta` VALUES (1086, 60, '_margin_right', '10');
INSERT INTO `piano_postmeta` VALUES (1087, 60, '_inifnity_loop', 'on');
INSERT INTO `piano_postmeta` VALUES (1088, 60, '_stage_padding', '0');
INSERT INTO `piano_postmeta` VALUES (1089, 60, '_auto_width', 'off');
INSERT INTO `piano_postmeta` VALUES (1090, 60, '_post_categories', '');
INSERT INTO `piano_postmeta` VALUES (1091, 60, '_post_tags', '');
INSERT INTO `piano_postmeta` VALUES (1092, 60, '_post_in', '');
INSERT INTO `piano_postmeta` VALUES (1093, 60, '_images_urls', 'a:1:{i:0;a:5:{s:3:\"url\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:8:\"link_url\";s:0:\"\";}}');
INSERT INTO `piano_postmeta` VALUES (1094, 60, '_edit_lock', '1527129369:1');
INSERT INTO `piano_postmeta` VALUES (1095, 63, '_wp_attached_file', '2018/05/banner-center.png');
INSERT INTO `piano_postmeta` VALUES (1096, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:310;s:4:\"file\";s:25:\"2018/05/banner-center.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"banner-center-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"banner-center-300x172.png\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"banner-center-324x310.png\";s:5:\"width\";i:324;s:6:\"height\";i:310;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"banner-center-416x239.png\";s:5:\"width\";i:416;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"banner-center-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"banner-center-324x310.png\";s:5:\"width\";i:324;s:6:\"height\";i:310;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"banner-center-416x239.png\";s:5:\"width\";i:416;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"banner-center-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1097, 64, '_wp_attached_file', '2018/05/banner-center@3x-1.png');
INSERT INTO `piano_postmeta` VALUES (1098, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1620;s:6:\"height\";i:930;s:4:\"file\";s:30:\"2018/05/banner-center@3x-1.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-300x172.png\";s:5:\"width\";i:300;s:6:\"height\";i:172;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-768x441.png\";s:5:\"width\";i:768;s:6:\"height\";i:441;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"banner-center@3x-1-1024x588.png\";s:5:\"width\";i:1024;s:6:\"height\";i:588;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"banner-center@3x-1-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-416x239.png\";s:5:\"width\";i:416;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-416x239.png\";s:5:\"width\";i:416;s:6:\"height\";i:239;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"banner-center@3x-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1099, 65, '_wp_attached_file', '2018/05/banner-right@3x-1.png');
INSERT INTO `piano_postmeta` VALUES (1100, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:900;s:6:\"height\";i:930;s:4:\"file\";s:29:\"2018/05/banner-right@3x-1.png\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-290x300.png\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-768x794.png\";s:5:\"width\";i:768;s:6:\"height\";i:794;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:29:\"banner-right@3x-1-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-416x430.png\";s:5:\"width\";i:416;s:6:\"height\";i:430;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-324x324.png\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-416x430.png\";s:5:\"width\";i:416;s:6:\"height\";i:430;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:29:\"banner-right@3x-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1101, 67, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1102, 67, '_edit_lock', '1527146445:1');
INSERT INTO `piano_postmeta` VALUES (1103, 70, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1104, 70, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1105, 70, '_menu_item_object_id', '32');
INSERT INTO `piano_postmeta` VALUES (1106, 70, '_menu_item_object', 'category');
INSERT INTO `piano_postmeta` VALUES (1107, 70, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1108, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1109, 70, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1110, 70, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1112, 71, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1113, 71, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1114, 71, '_menu_item_object_id', '31');
INSERT INTO `piano_postmeta` VALUES (1115, 71, '_menu_item_object', 'category');
INSERT INTO `piano_postmeta` VALUES (1116, 71, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1117, 71, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1118, 71, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1119, 71, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1121, 72, '_menu_item_type', 'custom');
INSERT INTO `piano_postmeta` VALUES (1122, 72, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1123, 72, '_menu_item_object_id', '72');
INSERT INTO `piano_postmeta` VALUES (1124, 72, '_menu_item_object', 'custom');
INSERT INTO `piano_postmeta` VALUES (1125, 72, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1126, 72, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1127, 72, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1128, 72, '_menu_item_url', 'http://localhost/wordpressWoo/wp-login.php');
INSERT INTO `piano_postmeta` VALUES (1130, 71, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1131, 71, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1132, 71, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1133, 71, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1134, 70, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1135, 70, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1136, 70, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1137, 70, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1138, 72, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1139, 72, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1140, 72, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1141, 72, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1142, 73, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1143, 73, '_edit_lock', '1527045812:1');
INSERT INTO `piano_postmeta` VALUES (1144, 73, '_wp_page_template', 'default');
INSERT INTO `piano_postmeta` VALUES (1145, 73, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1146, 73, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1147, 73, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1148, 73, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1149, 74, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1150, 74, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1151, 74, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1152, 74, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1153, 75, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1154, 75, '_edit_lock', '1527045834:1');
INSERT INTO `piano_postmeta` VALUES (1155, 75, '_wp_page_template', 'default');
INSERT INTO `piano_postmeta` VALUES (1156, 75, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1157, 75, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1158, 75, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1159, 75, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1160, 76, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1161, 76, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1162, 76, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1163, 76, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1164, 77, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1165, 77, '_edit_lock', '1527045906:1');
INSERT INTO `piano_postmeta` VALUES (1166, 77, '_wp_page_template', 'default');
INSERT INTO `piano_postmeta` VALUES (1167, 77, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1168, 77, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1169, 77, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1170, 77, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1171, 78, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1172, 78, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1173, 78, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1174, 78, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1175, 79, '_menu_item_type', 'post_type');
INSERT INTO `piano_postmeta` VALUES (1176, 79, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1177, 79, '_menu_item_object_id', '77');
INSERT INTO `piano_postmeta` VALUES (1178, 79, '_menu_item_object', 'page');
INSERT INTO `piano_postmeta` VALUES (1179, 79, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1180, 79, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1181, 79, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1182, 79, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1184, 80, '_menu_item_type', 'post_type');
INSERT INTO `piano_postmeta` VALUES (1185, 80, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1186, 80, '_menu_item_object_id', '75');
INSERT INTO `piano_postmeta` VALUES (1187, 80, '_menu_item_object', 'page');
INSERT INTO `piano_postmeta` VALUES (1188, 80, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1189, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1190, 80, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1191, 80, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1193, 81, '_menu_item_type', 'post_type');
INSERT INTO `piano_postmeta` VALUES (1194, 81, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1195, 81, '_menu_item_object_id', '73');
INSERT INTO `piano_postmeta` VALUES (1196, 81, '_menu_item_object', 'page');
INSERT INTO `piano_postmeta` VALUES (1197, 81, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1198, 81, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1199, 81, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1200, 81, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1202, 82, '_menu_item_type', 'post_type');
INSERT INTO `piano_postmeta` VALUES (1203, 82, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1204, 82, '_menu_item_object_id', '56');
INSERT INTO `piano_postmeta` VALUES (1205, 82, '_menu_item_object', 'page');
INSERT INTO `piano_postmeta` VALUES (1206, 82, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1207, 82, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1208, 82, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1209, 82, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1211, 82, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1212, 82, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1213, 82, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1214, 82, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1215, 81, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1216, 81, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1217, 81, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1218, 81, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1219, 80, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1220, 80, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1221, 80, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1222, 80, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1223, 79, 'email_heading', '');
INSERT INTO `piano_postmeta` VALUES (1224, 79, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_postmeta` VALUES (1225, 79, 'phone', '');
INSERT INTO `piano_postmeta` VALUES (1226, 79, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_postmeta` VALUES (1227, 90, '_wp_trash_meta_status', 'publish');
INSERT INTO `piano_postmeta` VALUES (1228, 90, '_wp_trash_meta_time', '1527070584');
INSERT INTO `piano_postmeta` VALUES (1229, 91, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1230, 91, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1231, 91, '_menu_item_object_id', '32');
INSERT INTO `piano_postmeta` VALUES (1232, 91, '_menu_item_object', 'category');
INSERT INTO `piano_postmeta` VALUES (1233, 91, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1234, 91, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1235, 91, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1236, 91, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1237, 91, '_menu_item_orphaned', '1527128133');
INSERT INTO `piano_postmeta` VALUES (1238, 92, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1239, 92, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1240, 92, '_menu_item_object_id', '1');
INSERT INTO `piano_postmeta` VALUES (1241, 92, '_menu_item_object', 'category');
INSERT INTO `piano_postmeta` VALUES (1242, 92, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1243, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1244, 92, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1245, 92, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1249, 98, '_wp_attached_file', '2018/05/ic-3-year-warrantly.png');
INSERT INTO `piano_postmeta` VALUES (1250, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:42;s:6:\"height\";i:45;s:4:\"file\";s:31:\"2018/05/ic-3-year-warrantly.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1251, 99, '_wp_attached_file', '2018/05/freeship.png');
INSERT INTO `piano_postmeta` VALUES (1252, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:42;s:6:\"height\";i:45;s:4:\"file\";s:20:\"2018/05/freeship.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1253, 100, '_wp_attached_file', '2018/05/ic-support.png');
INSERT INTO `piano_postmeta` VALUES (1254, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:42;s:6:\"height\";i:45;s:4:\"file\";s:22:\"2018/05/ic-support.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1255, 101, '_wp_attached_file', '2018/05/ic-big-store.png');
INSERT INTO `piano_postmeta` VALUES (1256, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:42;s:6:\"height\";i:45;s:4:\"file\";s:24:\"2018/05/ic-big-store.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1257, 107, '_wp_attached_file', '2018/05/ic-category-digital-piano.png');
INSERT INTO `piano_postmeta` VALUES (1258, 107, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:200;s:4:\"file\";s:37:\"2018/05/ic-category-digital-piano.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"ic-category-digital-piano-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:37:\"ic-category-digital-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:37:\"ic-category-digital-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1259, 108, '_wp_attached_file', '2018/05/ic-category-digital-piano-1.png');
INSERT INTO `piano_postmeta` VALUES (1260, 108, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:200;s:4:\"file\";s:39:\"2018/05/ic-category-digital-piano-1.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"ic-category-digital-piano-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:39:\"ic-category-digital-piano-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:39:\"ic-category-digital-piano-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1261, 109, '_wp_attached_file', '2018/05/ic-category-grand-piano.png');
INSERT INTO `piano_postmeta` VALUES (1262, 109, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:200;s:4:\"file\";s:35:\"2018/05/ic-category-grand-piano.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-grand-piano-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-grand-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-grand-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1263, 110, '_wp_attached_file', '2018/05/ic-category-stage-piano.png');
INSERT INTO `piano_postmeta` VALUES (1264, 110, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:200;s:4:\"file\";s:35:\"2018/05/ic-category-stage-piano.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-stage-piano-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-stage-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-stage-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1271, 114, '_wp_attached_file', '2018/05/ic-category-organ-3.png');
INSERT INTO `piano_postmeta` VALUES (1272, 114, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:200;s:4:\"file\";s:31:\"2018/05/ic-category-organ-3.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"ic-category-organ-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"ic-category-organ-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"ic-category-organ-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1273, 115, '_wp_attached_file', '2018/05/ic-category-cheap-piano.png');
INSERT INTO `piano_postmeta` VALUES (1274, 115, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:200;s:4:\"file\";s:35:\"2018/05/ic-category-cheap-piano.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-cheap-piano-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-cheap-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"ic-category-cheap-piano-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1275, 116, '_wp_attached_file', '2018/05/ic-category-piano-accessories.png');
INSERT INTO `piano_postmeta` VALUES (1276, 116, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:180;s:6:\"height\";i:200;s:4:\"file\";s:41:\"2018/05/ic-category-piano-accessories.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"ic-category-piano-accessories-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:41:\"ic-category-piano-accessories-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:41:\"ic-category-piano-accessories-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `piano_postmeta` VALUES (1277, 32, '_edit_lock', '1527146872:1');
INSERT INTO `piano_postmeta` VALUES (1278, 32, '_edit_last', '1');
INSERT INTO `piano_postmeta` VALUES (1279, 117, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1280, 117, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1281, 117, '_menu_item_object_id', '34');
INSERT INTO `piano_postmeta` VALUES (1282, 117, '_menu_item_object', 'product_cat');
INSERT INTO `piano_postmeta` VALUES (1283, 117, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1284, 117, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1285, 117, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1286, 117, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1288, 118, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1289, 118, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1290, 118, '_menu_item_object_id', '35');
INSERT INTO `piano_postmeta` VALUES (1291, 118, '_menu_item_object', 'product_cat');
INSERT INTO `piano_postmeta` VALUES (1292, 118, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1293, 118, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1294, 118, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1295, 118, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1297, 119, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1298, 119, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1299, 119, '_menu_item_object_id', '36');
INSERT INTO `piano_postmeta` VALUES (1300, 119, '_menu_item_object', 'product_cat');
INSERT INTO `piano_postmeta` VALUES (1301, 119, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1302, 119, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1303, 119, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1304, 119, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1306, 120, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1307, 120, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1308, 120, '_menu_item_object_id', '37');
INSERT INTO `piano_postmeta` VALUES (1309, 120, '_menu_item_object', 'product_cat');
INSERT INTO `piano_postmeta` VALUES (1310, 120, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1311, 120, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1312, 120, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1313, 120, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1315, 121, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1316, 121, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1317, 121, '_menu_item_object_id', '39');
INSERT INTO `piano_postmeta` VALUES (1318, 121, '_menu_item_object', 'product_cat');
INSERT INTO `piano_postmeta` VALUES (1319, 121, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1320, 121, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1321, 121, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1322, 121, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1324, 122, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1325, 122, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1326, 122, '_menu_item_object_id', '38');
INSERT INTO `piano_postmeta` VALUES (1327, 122, '_menu_item_object', 'product_cat');
INSERT INTO `piano_postmeta` VALUES (1328, 122, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1329, 122, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1330, 122, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1331, 122, '_menu_item_url', '');
INSERT INTO `piano_postmeta` VALUES (1333, 123, '_menu_item_type', 'taxonomy');
INSERT INTO `piano_postmeta` VALUES (1334, 123, '_menu_item_menu_item_parent', '0');
INSERT INTO `piano_postmeta` VALUES (1335, 123, '_menu_item_object_id', '40');
INSERT INTO `piano_postmeta` VALUES (1336, 123, '_menu_item_object', 'product_cat');
INSERT INTO `piano_postmeta` VALUES (1337, 123, '_menu_item_target', '');
INSERT INTO `piano_postmeta` VALUES (1338, 123, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}');
INSERT INTO `piano_postmeta` VALUES (1339, 123, '_menu_item_xfn', '');
INSERT INTO `piano_postmeta` VALUES (1340, 123, '_menu_item_url', '');

-- ----------------------------
-- Table structure for piano_posts
-- ----------------------------
DROP TABLE IF EXISTS `piano_posts`;
CREATE TABLE `piano_posts`  (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime(0) NOT NULL,
  `post_date_gmt` datetime(0) NOT NULL,
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime(0) NOT NULL,
  `post_modified_gmt` datetime(0) NOT NULL,
  `post_content_filtered` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `post_name`(`post_name`(191)) USING BTREE,
  INDEX `type_status_date`(`post_type`, `post_status`, `post_date`, `ID`) USING BTREE,
  INDEX `post_parent`(`post_parent`) USING BTREE,
  INDEX `post_author`(`post_author`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_posts
-- ----------------------------
INSERT INTO `piano_posts` VALUES (1, 1, '2018-05-16 03:29:56', '2018-05-16 03:29:56', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-05-16 03:29:56', '2018-05-16 03:29:56', '', 0, 'http://localhost/wordpressWoo/?p=1', 0, 'post', '', 1);
INSERT INTO `piano_posts` VALUES (2, 1, '2018-05-16 03:29:56', '2018-05-16 03:29:56', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\r\n\r\n...or something like this:\r\n\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\n\r\nAs a new WordPress user, you should go to <a href=\"http://localhost/wordpressWoo/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-05-16 03:29:56', '2018-05-16 03:29:56', '', 0, 'http://localhost/wordpressWoo/?page_id=2', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (4, 1, '2018-05-16 03:31:07', '2018-05-16 03:31:07', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2018-05-16 03:31:07', '2018-05-16 03:31:07', '', 0, 'http://localhost/wordpressWoo/shop/', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (5, 1, '2018-05-16 03:31:07', '2018-05-16 03:31:07', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-05-16 03:31:07', '2018-05-16 03:31:07', '', 0, 'http://localhost/wordpressWoo/cart/', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (6, 1, '2018-05-16 03:31:07', '2018-05-16 03:31:07', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2018-05-16 03:31:07', '2018-05-16 03:31:07', '', 0, 'http://localhost/wordpressWoo/checkout/', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (7, 1, '2018-05-16 03:31:07', '2018-05-16 03:31:07', '[woocommerce_my_account]', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2018-05-16 03:31:07', '2018-05-16 03:31:07', '', 0, 'http://localhost/wordpressWoo/my-account/', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (9, 1, '2018-05-16 03:33:50', '2018-05-16 03:33:50', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'V-Neck T-Shirt', 'This is a variable product.', 'publish', 'open', 'closed', '', 'v-neck-t-shirt', '', '', '2018-05-16 03:34:06', '2018-05-16 03:34:06', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-44/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (10, 1, '2018-05-16 03:33:50', '2018-05-16 03:33:50', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie', 'This is a variable product.', 'publish', 'open', 'closed', '', 'hoodie', '', '', '2018-05-16 03:34:14', '2018-05-16 03:34:14', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-45/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (11, 1, '2018-05-16 03:33:51', '2018-05-16 03:33:51', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Logo', 'This is a simple product.', 'publish', 'open', 'closed', '', 'hoodie-with-logo', '', '', '2018-05-16 03:34:15', '2018-05-16 03:34:15', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-46/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (12, 1, '2018-05-16 03:33:51', '2018-05-16 03:33:51', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'T-Shirt', 'This is a simple product.', 'publish', 'open', 'closed', '', 't-shirt', '', '', '2018-05-16 03:34:17', '2018-05-16 03:34:17', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-47/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (13, 1, '2018-05-16 03:33:52', '2018-05-16 03:33:52', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Beanie', 'This is a simple product.', 'publish', 'open', 'closed', '', 'beanie', '', '', '2018-05-16 03:34:19', '2018-05-16 03:34:19', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-48/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (14, 1, '2018-05-16 03:33:52', '2018-05-16 03:33:52', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Belt', 'This is a simple product.', 'publish', 'open', 'closed', '', 'belt', '', '', '2018-05-16 03:34:22', '2018-05-16 03:34:22', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-58/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (15, 1, '2018-05-16 03:33:53', '2018-05-16 03:33:53', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Cap', 'This is a simple product.', 'publish', 'open', 'closed', '', 'cap', '', '', '2018-05-16 03:34:24', '2018-05-16 03:34:24', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-60/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (16, 1, '2018-05-16 03:33:53', '2018-05-16 03:33:53', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Sunglasses', 'This is a simple product.', 'publish', 'open', 'closed', '', 'sunglasses', '', '', '2018-05-16 03:34:26', '2018-05-16 03:34:26', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-62/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (17, 1, '2018-05-16 03:33:53', '2018-05-16 03:33:53', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Pocket', 'This is a simple product.', 'publish', 'open', 'closed', '', 'hoodie-with-pocket', '', '', '2018-05-16 03:34:29', '2018-05-16 03:34:29', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-64/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (18, 1, '2018-05-16 03:33:53', '2018-05-16 03:33:53', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Zipper', 'This is a simple product.', 'publish', 'open', 'closed', '', 'hoodie-with-zipper', '', '', '2018-05-16 03:34:31', '2018-05-16 03:34:31', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-66/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (19, 1, '2018-05-16 03:33:54', '2018-05-16 03:33:54', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Long Sleeve Tee', 'This is a simple product.', 'publish', 'open', 'closed', '', 'long-sleeve-tee', '', '', '2018-05-16 03:34:34', '2018-05-16 03:34:34', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-68/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (20, 1, '2018-05-16 03:33:54', '2018-05-16 03:33:54', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Polo', 'This is a simple product.', 'publish', 'open', 'closed', '', 'polo', '', '', '2018-05-16 03:34:36', '2018-05-16 03:34:36', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-70/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (21, 1, '2018-05-16 03:33:54', '2018-05-16 03:33:54', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.', 'Album', 'This is a simple, virtual product.', 'publish', 'open', 'closed', '', 'album', '', '', '2018-05-16 03:34:39', '2018-05-16 03:34:39', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-73/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (22, 1, '2018-05-16 03:33:55', '2018-05-16 03:33:55', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.', 'Single', 'This is a simple, virtual product.', 'publish', 'open', 'closed', '', 'single', '', '', '2018-05-16 03:34:42', '2018-05-16 03:34:42', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-75/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (23, 1, '2018-05-16 03:33:55', '2018-05-16 03:33:55', '', 'V-Neck T-Shirt - Red', '', 'publish', 'closed', 'closed', '', 'v-neck-t-shirt-red', '', '', '2018-05-16 03:34:43', '2018-05-16 03:34:43', '', 9, 'http://localhost/wordpressWoo/product/import-placeholder-for-76/', 0, 'product_variation', '', 0);
INSERT INTO `piano_posts` VALUES (24, 1, '2018-05-16 03:33:55', '2018-05-16 03:33:55', '', 'V-Neck T-Shirt - Green', '', 'publish', 'closed', 'closed', '', 'v-neck-t-shirt-green', '', '', '2018-05-16 03:34:43', '2018-05-16 03:34:43', '', 9, 'http://localhost/wordpressWoo/product/import-placeholder-for-77/', 0, 'product_variation', '', 0);
INSERT INTO `piano_posts` VALUES (25, 1, '2018-05-16 03:33:56', '2018-05-16 03:33:56', '', 'V-Neck T-Shirt - Blue', '', 'publish', 'closed', 'closed', '', 'v-neck-t-shirt-blue', '', '', '2018-05-16 03:34:43', '2018-05-16 03:34:43', '', 9, 'http://localhost/wordpressWoo/product/import-placeholder-for-78/', 0, 'product_variation', '', 0);
INSERT INTO `piano_posts` VALUES (26, 1, '2018-05-16 03:33:56', '2018-05-16 03:33:56', '', 'Hoodie - Red, No', '', 'publish', 'closed', 'closed', '', 'hoodie-red-no', '', '', '2018-05-16 03:34:43', '2018-05-16 03:34:43', '', 10, 'http://localhost/wordpressWoo/product/import-placeholder-for-79/', 1, 'product_variation', '', 0);
INSERT INTO `piano_posts` VALUES (27, 1, '2018-05-16 03:33:56', '2018-05-16 03:33:56', '', 'Hoodie - Green, No', '', 'publish', 'closed', 'closed', '', 'hoodie-green-no', '', '', '2018-05-16 03:34:44', '2018-05-16 03:34:44', '', 10, 'http://localhost/wordpressWoo/product/import-placeholder-for-80/', 2, 'product_variation', '', 0);
INSERT INTO `piano_posts` VALUES (28, 1, '2018-05-16 03:33:57', '2018-05-16 03:33:57', '', 'Hoodie - Blue, No', '', 'publish', 'closed', 'closed', '', 'hoodie-blue-no', '', '', '2018-05-16 03:34:44', '2018-05-16 03:34:44', '', 10, 'http://localhost/wordpressWoo/product/import-placeholder-for-81/', 3, 'product_variation', '', 0);
INSERT INTO `piano_posts` VALUES (29, 1, '2018-05-16 03:33:57', '2018-05-16 03:33:57', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'T-Shirt with Logo', 'This is a simple product.', 'publish', 'open', 'closed', '', 't-shirt-with-logo', '', '', '2018-05-16 03:34:46', '2018-05-16 03:34:46', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-83/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (30, 1, '2018-05-16 03:33:57', '2018-05-16 03:33:57', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Beanie with Logo', 'This is a simple product.', 'publish', 'open', 'closed', '', 'beanie-with-logo', '', '', '2018-05-16 03:34:48', '2018-05-16 03:34:48', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-85/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (31, 1, '2018-05-16 03:33:58', '2018-05-16 03:33:58', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Logo Collection', 'This is a grouped product.', 'publish', 'open', 'closed', '', 'logo-collection', '', '', '2018-05-16 03:34:51', '2018-05-16 03:34:51', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-87/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (32, 1, '2018-05-16 03:33:58', '2018-05-16 03:33:58', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'WordPress Pennant', 'This is an external product.', 'publish', 'open', 'closed', '', 'wordpress-pennant', '', '', '2018-05-24 07:30:11', '2018-05-24 07:30:11', '', 0, 'http://localhost/wordpressWoo/product/import-placeholder-for-89/', 0, 'product', '', 0);
INSERT INTO `piano_posts` VALUES (33, 1, '2018-05-16 03:33:58', '2018-05-16 03:33:58', '', 'Hoodie - Blue, Yes', '', 'publish', 'closed', 'closed', '', 'hoodie-blue-yes', '', '', '2018-05-16 03:34:54', '2018-05-16 03:34:54', '', 10, 'http://localhost/wordpressWoo/product/import-placeholder-for-90/', 0, 'product_variation', '', 0);
INSERT INTO `piano_posts` VALUES (34, 1, '2018-05-16 03:34:01', '2018-05-16 03:34:01', '', 'vneck-tee-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'vneck-tee-2-1-jpg', '', '', '2018-05-16 03:34:01', '2018-05-16 03:34:01', '', 9, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/vneck-tee-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (35, 1, '2018-05-16 03:34:03', '2018-05-16 03:34:03', '', 'vnech-tee-green-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'vnech-tee-green-1-1-jpg', '', '', '2018-05-16 03:34:03', '2018-05-16 03:34:03', '', 9, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/vnech-tee-green-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (36, 1, '2018-05-16 03:34:05', '2018-05-16 03:34:05', '', 'vnech-tee-blue-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'vnech-tee-blue-1-1-jpg', '', '', '2018-05-16 03:34:05', '2018-05-16 03:34:05', '', 9, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/vnech-tee-blue-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (37, 1, '2018-05-16 03:34:08', '2018-05-16 03:34:08', '', 'hoodie-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-2-1-jpg', '', '', '2018-05-16 03:34:08', '2018-05-16 03:34:08', '', 10, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/hoodie-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (38, 1, '2018-05-16 03:34:10', '2018-05-16 03:34:10', '', 'hoodie-blue-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-blue-1-1-jpg', '', '', '2018-05-16 03:34:10', '2018-05-16 03:34:10', '', 10, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/hoodie-blue-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (39, 1, '2018-05-16 03:34:12', '2018-05-16 03:34:12', '', 'hoodie-green-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-green-1-1-jpg', '', '', '2018-05-16 03:34:12', '2018-05-16 03:34:12', '', 10, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/hoodie-green-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (40, 1, '2018-05-16 03:34:14', '2018-05-16 03:34:14', '', 'hoodie-with-logo-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-with-logo-2-1-jpg', '', '', '2018-05-16 03:34:14', '2018-05-16 03:34:14', '', 10, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/hoodie-with-logo-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (41, 1, '2018-05-16 03:34:17', '2018-05-16 03:34:17', '', 'tshirt-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'tshirt-2-1-jpg', '', '', '2018-05-16 03:34:17', '2018-05-16 03:34:17', '', 12, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/tshirt-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (42, 1, '2018-05-16 03:34:19', '2018-05-16 03:34:19', '', 'beanie-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'beanie-2-1-jpg', '', '', '2018-05-16 03:34:19', '2018-05-16 03:34:19', '', 13, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/beanie-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (43, 1, '2018-05-16 03:34:21', '2018-05-16 03:34:21', '', 'belt-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'belt-2-1-jpg', '', '', '2018-05-16 03:34:21', '2018-05-16 03:34:21', '', 14, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/belt-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (44, 1, '2018-05-16 03:34:23', '2018-05-16 03:34:23', '', 'cap-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'cap-2-1-jpg', '', '', '2018-05-16 03:34:23', '2018-05-16 03:34:23', '', 15, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/cap-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (45, 1, '2018-05-16 03:34:26', '2018-05-16 03:34:26', '', 'sunglasses-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'sunglasses-2-1-jpg', '', '', '2018-05-16 03:34:26', '2018-05-16 03:34:26', '', 16, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/sunglasses-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (46, 1, '2018-05-16 03:34:28', '2018-05-16 03:34:28', '', 'hoodie-with-pocket-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-with-pocket-2-1-jpg', '', '', '2018-05-16 03:34:28', '2018-05-16 03:34:28', '', 17, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/hoodie-with-pocket-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (47, 1, '2018-05-16 03:34:31', '2018-05-16 03:34:31', '', 'hoodie-with-zipper-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-with-zipper-2-1-jpg', '', '', '2018-05-16 03:34:31', '2018-05-16 03:34:31', '', 18, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/hoodie-with-zipper-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (48, 1, '2018-05-16 03:34:33', '2018-05-16 03:34:33', '', 'long-sleeve-tee-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'long-sleeve-tee-2-1-jpg', '', '', '2018-05-16 03:34:33', '2018-05-16 03:34:33', '', 19, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/long-sleeve-tee-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (49, 1, '2018-05-16 03:34:36', '2018-05-16 03:34:36', '', 'polo-2-1.jpg', '', 'inherit', 'open', 'closed', '', 'polo-2-1-jpg', '', '', '2018-05-16 03:34:36', '2018-05-16 03:34:36', '', 20, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/polo-2-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (50, 1, '2018-05-16 03:34:39', '2018-05-16 03:34:39', '', 'album-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'album-1-1-jpg', '', '', '2018-05-16 03:34:39', '2018-05-16 03:34:39', '', 21, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/album-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (51, 1, '2018-05-16 03:34:41', '2018-05-16 03:34:41', '', 'single-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'single-1-1-jpg', '', '', '2018-05-16 03:34:41', '2018-05-16 03:34:41', '', 22, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/single-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (52, 1, '2018-05-16 03:34:45', '2018-05-16 03:34:45', '', 't-shirt-with-logo-1-1.jpg', '', 'inherit', 'open', 'closed', '', 't-shirt-with-logo-1-1-jpg', '', '', '2018-05-16 03:34:45', '2018-05-16 03:34:45', '', 29, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/t-shirt-with-logo-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (53, 1, '2018-05-16 03:34:48', '2018-05-16 03:34:48', '', 'beanie-with-logo-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'beanie-with-logo-1-1-jpg', '', '', '2018-05-16 03:34:48', '2018-05-16 03:34:48', '', 30, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/beanie-with-logo-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (54, 1, '2018-05-16 03:34:50', '2018-05-16 03:34:50', '', 'logo-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'logo-1-1-jpg', '', '', '2018-05-16 03:34:50', '2018-05-16 03:34:50', '', 31, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/logo-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (55, 1, '2018-05-16 03:34:53', '2018-05-16 03:34:53', '', 'pennant-1-1.jpg', '', 'inherit', 'open', 'closed', '', 'pennant-1-1-jpg', '', '', '2018-05-16 03:34:53', '2018-05-16 03:34:53', '', 32, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/pennant-1-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `piano_posts` VALUES (56, 1, '2018-05-16 03:41:18', '2018-05-16 03:41:18', '', 'home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-05-16 03:41:18', '2018-05-16 03:41:18', '', 0, 'http://localhost/wordpressWoo/?page_id=56', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (57, 1, '2018-05-16 03:41:18', '2018-05-16 03:41:18', '', 'home', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2018-05-16 03:41:18', '2018-05-16 03:41:18', '', 56, 'http://localhost/wordpressWoo/2018/05/16/56-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `piano_posts` VALUES (58, 1, '2018-05-16 03:41:27', '2018-05-16 03:41:27', '', 'blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-05-16 03:41:27', '2018-05-16 03:41:27', '', 0, 'http://localhost/wordpressWoo/?page_id=58', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (59, 1, '2018-05-16 03:41:27', '2018-05-16 03:41:27', '', 'blog', '', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2018-05-16 03:41:27', '2018-05-16 03:41:27', '', 58, 'http://localhost/wordpressWoo/2018/05/16/58-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `piano_posts` VALUES (60, 1, '2018-05-21 08:03:28', '2018-05-21 08:03:28', '', 'Slider home', '', 'publish', 'closed', 'closed', '', '60', '', '', '2018-05-21 08:18:18', '2018-05-21 08:18:18', '', 0, 'http://localhost/wordpressWoo/?post_type=carousels&#038;p=60', 0, 'carousels', '', 0);
INSERT INTO `piano_posts` VALUES (61, 1, '2018-05-21 08:02:39', '2018-05-21 08:02:39', '', 'banner-center@3x', '', 'inherit', 'open', 'closed', '', 'banner-center3x', '', '', '2018-05-21 08:02:39', '2018-05-21 08:02:39', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/banner-center@3x.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (62, 1, '2018-05-21 08:02:41', '2018-05-21 08:02:41', '', 'banner-right@3x', '', 'inherit', 'open', 'closed', '', 'banner-right3x', '', '', '2018-05-21 08:02:41', '2018-05-21 08:02:41', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/banner-right@3x.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (63, 1, '2018-05-21 08:16:34', '2018-05-21 08:16:34', '', 'banner-center', '', 'inherit', 'open', 'closed', '', 'banner-center', '', '', '2018-05-21 08:16:34', '2018-05-21 08:16:34', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/banner-center.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (64, 1, '2018-05-21 08:16:59', '2018-05-21 08:16:59', '', 'banner-center@3x', '', 'inherit', 'open', 'closed', '', 'banner-center3x-2', '', '', '2018-05-21 08:16:59', '2018-05-21 08:16:59', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/banner-center@3x-1.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (65, 1, '2018-05-21 08:17:00', '2018-05-21 08:17:00', '', 'banner-right@3x', '', 'inherit', 'open', 'closed', '', 'banner-right3x-2', '', '', '2018-05-21 08:17:00', '2018-05-21 08:17:00', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/banner-right@3x-1.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (66, 1, '2018-05-23 03:07:38', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-05-23 03:07:38', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpressWoo/?p=66', 0, 'post', '', 0);
INSERT INTO `piano_posts` VALUES (67, 1, '2018-05-23 03:17:09', '2018-05-23 03:17:09', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"Peocandy-theme\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Home Options', 'home-options', 'publish', 'closed', 'closed', '', 'group_5b04dce3e7f16', '', '', '2018-05-24 07:23:02', '2018-05-24 07:23:02', '', 0, 'http://localhost/wordpressWoo/?post_type=acf-field-group&#038;p=67', 0, 'acf-field-group', '', 0);
INSERT INTO `piano_posts` VALUES (70, 1, '2018-05-23 03:24:33', '2018-05-23 03:24:33', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2018-05-23 03:24:33', '2018-05-23 03:24:33', '', 0, 'http://localhost/wordpressWoo/?p=70', 2, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (71, 1, '2018-05-23 03:24:33', '2018-05-23 03:24:33', ' ', '', '', 'publish', 'closed', 'closed', '', '71', '', '', '2018-05-23 03:24:33', '2018-05-23 03:24:33', '', 0, 'http://localhost/wordpressWoo/?p=71', 1, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (72, 1, '2018-05-23 03:24:33', '2018-05-23 03:24:33', '', 'Đăng nhập', '', 'publish', 'closed', 'closed', '', 'dang-nhap', '', '', '2018-05-23 03:24:33', '2018-05-23 03:24:33', '', 0, 'http://localhost/wordpressWoo/?p=72', 3, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (73, 1, '2018-05-23 03:25:41', '2018-05-23 03:25:41', 'Đây là trang giới thiệu', 'Giới thiệu', '', 'publish', 'closed', 'closed', '', 'gioi-thieu', '', '', '2018-05-23 03:25:41', '2018-05-23 03:25:41', '', 0, 'http://localhost/wordpressWoo/?page_id=73', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (74, 1, '2018-05-23 03:25:41', '2018-05-23 03:25:41', 'Đây là trang giới thiệu', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-05-23 03:25:41', '2018-05-23 03:25:41', '', 73, 'http://localhost/wordpressWoo/2018/05/23/73-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `piano_posts` VALUES (75, 1, '2018-05-23 03:26:10', '2018-05-23 03:26:10', 'Đây là trang liên hệ', 'Liên hệ', '', 'publish', 'closed', 'closed', '', 'lien-he', '', '', '2018-05-23 03:26:10', '2018-05-23 03:26:10', '', 0, 'http://localhost/wordpressWoo/?page_id=75', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (76, 1, '2018-05-23 03:26:10', '2018-05-23 03:26:10', 'Đây là trang liên hệ', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2018-05-23 03:26:10', '2018-05-23 03:26:10', '', 75, 'http://localhost/wordpressWoo/2018/05/23/75-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `piano_posts` VALUES (77, 1, '2018-05-23 03:26:28', '2018-05-23 03:26:28', 'Đây là trang tư vấn', 'Tư vấn', '', 'publish', 'closed', 'closed', '', 'tu-van', '', '', '2018-05-23 03:26:28', '2018-05-23 03:26:28', '', 0, 'http://localhost/wordpressWoo/?page_id=77', 0, 'page', '', 0);
INSERT INTO `piano_posts` VALUES (78, 1, '2018-05-23 03:26:28', '2018-05-23 03:26:28', 'Đây là trang tư vấn', 'Tư vấn', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2018-05-23 03:26:28', '2018-05-23 03:26:28', '', 77, 'http://localhost/wordpressWoo/2018/05/23/77-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `piano_posts` VALUES (79, 1, '2018-05-23 03:27:25', '2018-05-23 03:27:25', ' ', '', '', 'publish', 'closed', 'closed', '', '79', '', '', '2018-05-24 02:16:07', '2018-05-24 02:16:07', '', 0, 'http://localhost/wordpressWoo/?p=79', 5, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (80, 1, '2018-05-23 03:27:25', '2018-05-23 03:27:25', ' ', '', '', 'publish', 'closed', 'closed', '', '80', '', '', '2018-05-24 02:16:07', '2018-05-24 02:16:07', '', 0, 'http://localhost/wordpressWoo/?p=80', 4, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (81, 1, '2018-05-23 03:27:25', '2018-05-23 03:27:25', ' ', '', '', 'publish', 'closed', 'closed', '', '81', '', '', '2018-05-24 02:16:07', '2018-05-24 02:16:07', '', 0, 'http://localhost/wordpressWoo/?p=81', 2, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (82, 1, '2018-05-23 03:27:25', '2018-05-23 03:27:25', '', 'Trang chủ', '', 'publish', 'closed', 'closed', '', 'trang-chu', '', '', '2018-05-24 02:16:07', '2018-05-24 02:16:07', '', 0, 'http://localhost/wordpressWoo/?p=82', 1, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (83, 1, '2018-05-23 08:38:34', '2018-05-23 08:38:34', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Home setting', '', 'publish', 'closed', 'closed', '', 'field_5b0527e53acd8', '', '', '2018-05-23 08:38:34', '2018-05-23 08:38:34', '', 67, 'http://localhost/wordpressWoo/?post_type=acf-field&p=83', 0, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (84, 1, '2018-05-23 08:38:34', '2018-05-23 08:38:34', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:24:\"Display email in top nav\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Email', 'menu_top_email', 'publish', 'closed', 'closed', '', 'field_5b0528123acd9', '', '', '2018-05-23 08:38:34', '2018-05-23 08:38:34', '', 67, 'http://localhost/wordpressWoo/?post_type=acf-field&p=84', 1, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (85, 1, '2018-05-23 08:38:34', '2018-05-23 08:38:34', 'a:12:{s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:32:\"Display phone number in top Menu\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";}', 'Phone', 'menu_top_phone', 'publish', 'closed', 'closed', '', 'field_5b05283b3acda', '', '', '2018-05-23 08:49:19', '2018-05-23 08:49:19', '', 67, 'http://localhost/wordpressWoo/?post_type=acf-field&#038;p=85', 2, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (86, 1, '2018-05-23 08:38:34', '2018-05-23 08:38:34', 'a:12:{s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:34:\"Display Hotline number in top Menu\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";}', 'Hotline', 'menu_top_hotline', 'publish', 'closed', 'closed', '', 'field_5b0528663acdb', '', '', '2018-05-23 08:49:19', '2018-05-23 08:49:19', '', 67, 'http://localhost/wordpressWoo/?post_type=acf-field&#038;p=86', 3, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (87, 1, '2018-05-23 08:38:34', '2018-05-23 08:38:34', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Page setting', '', 'publish', 'closed', 'closed', '', 'field_5b0528773acdc', '', '', '2018-05-23 08:56:08', '2018-05-23 08:56:08', '', 67, 'http://localhost/wordpressWoo/?post_type=acf-field&#038;p=87', 4, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (90, 1, '2018-05-23 10:16:24', '2018-05-23 10:16:24', '{\n    \"Peocandy::nav_menu_locations[secondary]\": {\n        \"value\": 30,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-05-23 10:16:24\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '57dd091c-c75d-4273-aa17-fe7d789ae005', '', '', '2018-05-23 10:16:24', '2018-05-23 10:16:24', '', 0, 'http://localhost/wordpressWoo/2018/05/23/57dd091c-c75d-4273-aa17-fe7d789ae005/', 0, 'customize_changeset', '', 0);
INSERT INTO `piano_posts` VALUES (91, 1, '2018-05-24 02:15:33', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-05-24 02:15:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpressWoo/?p=91', 1, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (92, 1, '2018-05-24 02:16:07', '2018-05-24 02:16:07', '', 'Sản phẩm', '', 'publish', 'closed', 'closed', '', 'san-pham', '', '', '2018-05-24 02:16:07', '2018-05-24 02:16:07', '', 0, 'http://localhost/wordpressWoo/?p=92', 3, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (93, 1, '2018-05-24 02:19:08', '2018-05-24 02:19:08', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";i:4;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:11:\"Add Service\";}', 'Services', 'service_content', 'publish', 'closed', 'closed', '', 'field_5b062095e6faf', '', '', '2018-05-24 04:32:21', '2018-05-24 04:32:21', '', 67, 'http://localhost/wordpressWoo/?post_type=acf-field&#038;p=93', 5, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (94, 1, '2018-05-24 02:19:08', '2018-05-24 02:19:08', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Service Name', 'service_name', 'publish', 'closed', 'closed', '', 'field_5b0620b8e6fb0', '', '', '2018-05-24 02:19:08', '2018-05-24 02:19:08', '', 93, 'http://localhost/wordpressWoo/?post_type=acf-field&p=94', 0, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (95, 1, '2018-05-24 02:19:08', '2018-05-24 02:19:08', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Service Icon', 'service_icon', 'publish', 'closed', 'closed', '', 'field_5b0620cbe6fb1', '', '', '2018-05-24 02:19:08', '2018-05-24 02:19:08', '', 93, 'http://localhost/wordpressWoo/?post_type=acf-field&p=95', 1, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (96, 1, '2018-05-24 02:19:08', '2018-05-24 02:19:08', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Service link', 'service_link', 'publish', 'closed', 'closed', '', 'field_5b0620e7e6fb2', '', '', '2018-05-24 02:19:08', '2018-05-24 02:19:08', '', 93, 'http://localhost/wordpressWoo/?post_type=acf-field&p=96', 2, 'acf-field', '', 0);
INSERT INTO `piano_posts` VALUES (98, 1, '2018-05-24 02:21:37', '2018-05-24 02:21:37', '', 'ic-3-year-warrantly', '', 'inherit', 'open', 'closed', '', 'ic-3-year-warrantly', '', '', '2018-05-24 02:21:37', '2018-05-24 02:21:37', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-3-year-warrantly.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (99, 1, '2018-05-24 02:22:30', '2018-05-24 02:22:30', '', 'freeship', '', 'inherit', 'open', 'closed', '', 'freeship', '', '', '2018-05-24 02:22:30', '2018-05-24 02:22:30', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/freeship.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (100, 1, '2018-05-24 02:23:01', '2018-05-24 02:23:01', '', 'ic-support', '', 'inherit', 'open', 'closed', '', 'ic-support', '', '', '2018-05-24 02:23:01', '2018-05-24 02:23:01', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-support.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (101, 1, '2018-05-24 02:23:46', '2018-05-24 02:23:46', '', 'ic-big-store', '', 'inherit', 'open', 'closed', '', 'ic-big-store', '', '', '2018-05-24 02:23:46', '2018-05-24 02:23:46', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-big-store.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (107, 1, '2018-05-24 06:46:23', '2018-05-24 06:46:23', '', 'ic-category-digital-piano', '', 'inherit', 'open', 'closed', '', 'ic-category-digital-piano', '', '', '2018-05-24 06:46:23', '2018-05-24 06:46:23', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-category-digital-piano.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (108, 1, '2018-05-24 06:48:03', '2018-05-24 06:48:03', '', 'ic-category-digital-piano', '', 'inherit', 'open', 'closed', '', 'ic-category-digital-piano-2', '', '', '2018-05-24 06:48:03', '2018-05-24 06:48:03', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-category-digital-piano-1.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (109, 1, '2018-05-24 06:58:53', '2018-05-24 06:58:53', '', 'ic-category-grand-piano', '', 'inherit', 'open', 'closed', '', 'ic-category-grand-piano', '', '', '2018-05-24 06:58:53', '2018-05-24 06:58:53', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-category-grand-piano.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (110, 1, '2018-05-24 07:01:35', '2018-05-24 07:01:35', '', 'ic-category-stage-piano', '', 'inherit', 'open', 'closed', '', 'ic-category-stage-piano', '', '', '2018-05-24 07:01:35', '2018-05-24 07:01:35', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-category-stage-piano.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (114, 1, '2018-05-24 07:07:38', '2018-05-24 07:07:38', '', 'ic-category-organ', '', 'inherit', 'open', 'closed', '', 'ic-category-organ-4', '', '', '2018-05-24 07:07:38', '2018-05-24 07:07:38', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-category-organ-3.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (115, 1, '2018-05-24 07:09:18', '2018-05-24 07:09:18', '', 'ic-category-cheap-piano', '', 'inherit', 'open', 'closed', '', 'ic-category-cheap-piano', '', '', '2018-05-24 07:09:18', '2018-05-24 07:09:18', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-category-cheap-piano.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (116, 1, '2018-05-24 07:11:36', '2018-05-24 07:11:36', '', 'ic-category-piano-accessories', '', 'inherit', 'open', 'closed', '', 'ic-category-piano-accessories', '', '', '2018-05-24 07:11:36', '2018-05-24 07:11:36', '', 0, 'http://localhost/wordpressWoo/wp-content/uploads/2018/05/ic-category-piano-accessories.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `piano_posts` VALUES (117, 1, '2018-05-24 07:36:48', '2018-05-24 07:36:48', ' ', '', '', 'publish', 'closed', 'closed', '', '117', '', '', '2018-05-24 07:36:48', '2018-05-24 07:36:48', '', 0, 'http://localhost/wordpressWoo/?p=117', 1, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (118, 1, '2018-05-24 07:36:48', '2018-05-24 07:36:48', ' ', '', '', 'publish', 'closed', 'closed', '', '118', '', '', '2018-05-24 07:36:48', '2018-05-24 07:36:48', '', 0, 'http://localhost/wordpressWoo/?p=118', 2, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (119, 1, '2018-05-24 07:36:48', '2018-05-24 07:36:48', ' ', '', '', 'publish', 'closed', 'closed', '', '119', '', '', '2018-05-24 07:36:48', '2018-05-24 07:36:48', '', 0, 'http://localhost/wordpressWoo/?p=119', 3, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (120, 1, '2018-05-24 07:36:48', '2018-05-24 07:36:48', ' ', '', '', 'publish', 'closed', 'closed', '', '120', '', '', '2018-05-24 07:36:48', '2018-05-24 07:36:48', '', 0, 'http://localhost/wordpressWoo/?p=120', 4, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (121, 1, '2018-05-24 07:36:48', '2018-05-24 07:36:48', ' ', '', '', 'publish', 'closed', 'closed', '', '121', '', '', '2018-05-24 07:36:48', '2018-05-24 07:36:48', '', 0, 'http://localhost/wordpressWoo/?p=121', 5, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (122, 1, '2018-05-24 07:36:48', '2018-05-24 07:36:48', ' ', '', '', 'publish', 'closed', 'closed', '', '122', '', '', '2018-05-24 07:36:48', '2018-05-24 07:36:48', '', 0, 'http://localhost/wordpressWoo/?p=122', 6, 'nav_menu_item', '', 0);
INSERT INTO `piano_posts` VALUES (123, 1, '2018-05-24 07:36:48', '2018-05-24 07:36:48', ' ', '', '', 'publish', 'closed', 'closed', '', '123', '', '', '2018-05-24 07:36:48', '2018-05-24 07:36:48', '', 0, 'http://localhost/wordpressWoo/?p=123', 7, 'nav_menu_item', '', 0);

-- ----------------------------
-- Table structure for piano_term_relationships
-- ----------------------------
DROP TABLE IF EXISTS `piano_term_relationships`;
CREATE TABLE `piano_term_relationships`  (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`object_id`, `term_taxonomy_id`) USING BTREE,
  INDEX `term_taxonomy_id`(`term_taxonomy_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_term_relationships
-- ----------------------------
INSERT INTO `piano_term_relationships` VALUES (1, 1, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 4, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 8, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 17, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 22, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 23, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 24, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 25, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 26, 0);
INSERT INTO `piano_term_relationships` VALUES (9, 27, 0);
INSERT INTO `piano_term_relationships` VALUES (10, 4, 0);
INSERT INTO `piano_term_relationships` VALUES (10, 18, 0);
INSERT INTO `piano_term_relationships` VALUES (10, 22, 0);
INSERT INTO `piano_term_relationships` VALUES (10, 23, 0);
INSERT INTO `piano_term_relationships` VALUES (10, 24, 0);
INSERT INTO `piano_term_relationships` VALUES (11, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (11, 18, 0);
INSERT INTO `piano_term_relationships` VALUES (11, 22, 0);
INSERT INTO `piano_term_relationships` VALUES (12, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (12, 17, 0);
INSERT INTO `piano_term_relationships` VALUES (12, 28, 0);
INSERT INTO `piano_term_relationships` VALUES (13, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (13, 19, 0);
INSERT INTO `piano_term_relationships` VALUES (13, 24, 0);
INSERT INTO `piano_term_relationships` VALUES (14, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (14, 19, 0);
INSERT INTO `piano_term_relationships` VALUES (15, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (15, 8, 0);
INSERT INTO `piano_term_relationships` VALUES (15, 19, 0);
INSERT INTO `piano_term_relationships` VALUES (15, 29, 0);
INSERT INTO `piano_term_relationships` VALUES (16, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (16, 8, 0);
INSERT INTO `piano_term_relationships` VALUES (16, 19, 0);
INSERT INTO `piano_term_relationships` VALUES (17, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (17, 6, 0);
INSERT INTO `piano_term_relationships` VALUES (17, 7, 0);
INSERT INTO `piano_term_relationships` VALUES (17, 8, 0);
INSERT INTO `piano_term_relationships` VALUES (17, 18, 0);
INSERT INTO `piano_term_relationships` VALUES (17, 28, 0);
INSERT INTO `piano_term_relationships` VALUES (18, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (18, 8, 0);
INSERT INTO `piano_term_relationships` VALUES (18, 18, 0);
INSERT INTO `piano_term_relationships` VALUES (19, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (19, 17, 0);
INSERT INTO `piano_term_relationships` VALUES (19, 23, 0);
INSERT INTO `piano_term_relationships` VALUES (20, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (20, 17, 0);
INSERT INTO `piano_term_relationships` VALUES (20, 22, 0);
INSERT INTO `piano_term_relationships` VALUES (21, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (21, 20, 0);
INSERT INTO `piano_term_relationships` VALUES (22, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (22, 20, 0);
INSERT INTO `piano_term_relationships` VALUES (23, 15, 0);
INSERT INTO `piano_term_relationships` VALUES (24, 15, 0);
INSERT INTO `piano_term_relationships` VALUES (25, 15, 0);
INSERT INTO `piano_term_relationships` VALUES (26, 15, 0);
INSERT INTO `piano_term_relationships` VALUES (27, 15, 0);
INSERT INTO `piano_term_relationships` VALUES (28, 15, 0);
INSERT INTO `piano_term_relationships` VALUES (29, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (29, 17, 0);
INSERT INTO `piano_term_relationships` VALUES (29, 28, 0);
INSERT INTO `piano_term_relationships` VALUES (30, 2, 0);
INSERT INTO `piano_term_relationships` VALUES (30, 19, 0);
INSERT INTO `piano_term_relationships` VALUES (30, 24, 0);
INSERT INTO `piano_term_relationships` VALUES (31, 3, 0);
INSERT INTO `piano_term_relationships` VALUES (31, 16, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 5, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 16, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 20, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 21, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 34, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 35, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 36, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 37, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 38, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 39, 0);
INSERT INTO `piano_term_relationships` VALUES (32, 40, 0);
INSERT INTO `piano_term_relationships` VALUES (33, 15, 0);
INSERT INTO `piano_term_relationships` VALUES (70, 30, 0);
INSERT INTO `piano_term_relationships` VALUES (71, 30, 0);
INSERT INTO `piano_term_relationships` VALUES (72, 30, 0);
INSERT INTO `piano_term_relationships` VALUES (79, 33, 0);
INSERT INTO `piano_term_relationships` VALUES (80, 33, 0);
INSERT INTO `piano_term_relationships` VALUES (81, 33, 0);
INSERT INTO `piano_term_relationships` VALUES (82, 33, 0);
INSERT INTO `piano_term_relationships` VALUES (92, 33, 0);
INSERT INTO `piano_term_relationships` VALUES (117, 41, 0);
INSERT INTO `piano_term_relationships` VALUES (118, 41, 0);
INSERT INTO `piano_term_relationships` VALUES (119, 41, 0);
INSERT INTO `piano_term_relationships` VALUES (120, 41, 0);
INSERT INTO `piano_term_relationships` VALUES (121, 41, 0);
INSERT INTO `piano_term_relationships` VALUES (122, 41, 0);
INSERT INTO `piano_term_relationships` VALUES (123, 41, 0);

-- ----------------------------
-- Table structure for piano_term_taxonomy
-- ----------------------------
DROP TABLE IF EXISTS `piano_term_taxonomy`;
CREATE TABLE `piano_term_taxonomy`  (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_taxonomy_id`) USING BTREE,
  UNIQUE INDEX `term_id_taxonomy`(`term_id`, `taxonomy`) USING BTREE,
  INDEX `taxonomy`(`taxonomy`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_term_taxonomy
-- ----------------------------
INSERT INTO `piano_term_taxonomy` VALUES (1, 1, 'category', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (2, 2, 'product_type', '', 0, 14);
INSERT INTO `piano_term_taxonomy` VALUES (3, 3, 'product_type', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (4, 4, 'product_type', '', 0, 2);
INSERT INTO `piano_term_taxonomy` VALUES (5, 5, 'product_type', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (6, 6, 'product_visibility', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (7, 7, 'product_visibility', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (8, 8, 'product_visibility', '', 0, 5);
INSERT INTO `piano_term_taxonomy` VALUES (9, 9, 'product_visibility', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (10, 10, 'product_visibility', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (11, 11, 'product_visibility', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (12, 12, 'product_visibility', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (13, 13, 'product_visibility', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (14, 14, 'product_visibility', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (15, 15, 'product_cat', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (16, 16, 'product_cat', '', 0, 2);
INSERT INTO `piano_term_taxonomy` VALUES (17, 17, 'product_cat', '', 16, 5);
INSERT INTO `piano_term_taxonomy` VALUES (18, 18, 'product_cat', '', 16, 4);
INSERT INTO `piano_term_taxonomy` VALUES (19, 19, 'product_cat', '', 16, 5);
INSERT INTO `piano_term_taxonomy` VALUES (20, 20, 'product_cat', '', 0, 3);
INSERT INTO `piano_term_taxonomy` VALUES (21, 21, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (22, 22, 'pa_color', '', 0, 4);
INSERT INTO `piano_term_taxonomy` VALUES (23, 23, 'pa_color', '', 0, 3);
INSERT INTO `piano_term_taxonomy` VALUES (24, 24, 'pa_color', '', 0, 4);
INSERT INTO `piano_term_taxonomy` VALUES (25, 25, 'pa_size', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (26, 26, 'pa_size', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (27, 27, 'pa_size', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (28, 28, 'pa_color', '', 0, 3);
INSERT INTO `piano_term_taxonomy` VALUES (29, 29, 'pa_color', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (30, 30, 'nav_menu', '', 0, 3);
INSERT INTO `piano_term_taxonomy` VALUES (31, 31, 'category', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (32, 32, 'category', '', 0, 0);
INSERT INTO `piano_term_taxonomy` VALUES (33, 33, 'nav_menu', '', 0, 5);
INSERT INTO `piano_term_taxonomy` VALUES (34, 34, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (35, 35, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (36, 36, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (37, 37, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (38, 38, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (39, 39, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (40, 40, 'product_cat', '', 0, 1);
INSERT INTO `piano_term_taxonomy` VALUES (41, 41, 'nav_menu', '', 0, 7);
INSERT INTO `piano_term_taxonomy` VALUES (42, 42, 'product_cat', '', 0, 0);

-- ----------------------------
-- Table structure for piano_termmeta
-- ----------------------------
DROP TABLE IF EXISTS `piano_termmeta`;
CREATE TABLE `piano_termmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `term_id`(`term_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_termmeta
-- ----------------------------
INSERT INTO `piano_termmeta` VALUES (1, 15, 'product_count_product_cat', '0');
INSERT INTO `piano_termmeta` VALUES (2, 16, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (3, 17, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (4, 18, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (5, 19, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (6, 20, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (7, 21, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (8, 17, 'product_count_product_cat', '5');
INSERT INTO `piano_termmeta` VALUES (9, 16, 'product_count_product_cat', '15');
INSERT INTO `piano_termmeta` VALUES (10, 22, 'order_pa_color', '0');
INSERT INTO `piano_termmeta` VALUES (11, 23, 'order_pa_color', '0');
INSERT INTO `piano_termmeta` VALUES (12, 24, 'order_pa_color', '0');
INSERT INTO `piano_termmeta` VALUES (13, 25, 'order_pa_size', '0');
INSERT INTO `piano_termmeta` VALUES (14, 26, 'order_pa_size', '0');
INSERT INTO `piano_termmeta` VALUES (15, 27, 'order_pa_size', '0');
INSERT INTO `piano_termmeta` VALUES (16, 18, 'product_count_product_cat', '3');
INSERT INTO `piano_termmeta` VALUES (17, 28, 'order_pa_color', '0');
INSERT INTO `piano_termmeta` VALUES (18, 19, 'product_count_product_cat', '5');
INSERT INTO `piano_termmeta` VALUES (19, 29, 'order_pa_color', '0');
INSERT INTO `piano_termmeta` VALUES (20, 20, 'product_count_product_cat', '3');
INSERT INTO `piano_termmeta` VALUES (21, 21, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (22, 31, 'email_heading', '');
INSERT INTO `piano_termmeta` VALUES (23, 31, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_termmeta` VALUES (24, 31, 'phone', '');
INSERT INTO `piano_termmeta` VALUES (25, 31, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_termmeta` VALUES (26, 32, 'email_heading', '');
INSERT INTO `piano_termmeta` VALUES (27, 32, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_termmeta` VALUES (28, 32, 'phone', '');
INSERT INTO `piano_termmeta` VALUES (29, 32, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_termmeta` VALUES (30, 30, 'email_heading', '');
INSERT INTO `piano_termmeta` VALUES (31, 30, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_termmeta` VALUES (32, 30, 'phone', '');
INSERT INTO `piano_termmeta` VALUES (33, 30, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_termmeta` VALUES (34, 33, 'email_heading', '');
INSERT INTO `piano_termmeta` VALUES (35, 33, '_email_heading', 'field_5b04dd01489d1');
INSERT INTO `piano_termmeta` VALUES (36, 33, 'phone', '');
INSERT INTO `piano_termmeta` VALUES (37, 33, '_phone', 'field_5b04dd2c489d2');
INSERT INTO `piano_termmeta` VALUES (38, 34, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (39, 34, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (40, 34, 'thumbnail_id', '107');
INSERT INTO `piano_termmeta` VALUES (41, 35, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (42, 35, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (43, 35, 'thumbnail_id', '108');
INSERT INTO `piano_termmeta` VALUES (44, 36, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (45, 36, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (46, 36, 'thumbnail_id', '109');
INSERT INTO `piano_termmeta` VALUES (47, 37, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (48, 37, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (49, 37, 'thumbnail_id', '110');
INSERT INTO `piano_termmeta` VALUES (50, 38, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (51, 38, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (52, 38, 'thumbnail_id', '114');
INSERT INTO `piano_termmeta` VALUES (53, 39, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (54, 39, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (55, 39, 'thumbnail_id', '115');
INSERT INTO `piano_termmeta` VALUES (56, 40, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (57, 40, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (58, 40, 'thumbnail_id', '116');
INSERT INTO `piano_termmeta` VALUES (59, 39, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (60, 36, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (61, 38, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (62, 40, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (63, 34, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (64, 37, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (65, 35, 'product_count_product_cat', '1');
INSERT INTO `piano_termmeta` VALUES (66, 42, 'order', '0');
INSERT INTO `piano_termmeta` VALUES (67, 42, 'display_type', '');
INSERT INTO `piano_termmeta` VALUES (68, 42, 'thumbnail_id', '48');

-- ----------------------------
-- Table structure for piano_terms
-- ----------------------------
DROP TABLE IF EXISTS `piano_terms`;
CREATE TABLE `piano_terms`  (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_id`) USING BTREE,
  INDEX `slug`(`slug`(191)) USING BTREE,
  INDEX `name`(`name`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_terms
-- ----------------------------
INSERT INTO `piano_terms` VALUES (1, 'Uncategorized', 'uncategorized', 0);
INSERT INTO `piano_terms` VALUES (2, 'simple', 'simple', 0);
INSERT INTO `piano_terms` VALUES (3, 'grouped', 'grouped', 0);
INSERT INTO `piano_terms` VALUES (4, 'variable', 'variable', 0);
INSERT INTO `piano_terms` VALUES (5, 'external', 'external', 0);
INSERT INTO `piano_terms` VALUES (6, 'exclude-from-search', 'exclude-from-search', 0);
INSERT INTO `piano_terms` VALUES (7, 'exclude-from-catalog', 'exclude-from-catalog', 0);
INSERT INTO `piano_terms` VALUES (8, 'featured', 'featured', 0);
INSERT INTO `piano_terms` VALUES (9, 'outofstock', 'outofstock', 0);
INSERT INTO `piano_terms` VALUES (10, 'rated-1', 'rated-1', 0);
INSERT INTO `piano_terms` VALUES (11, 'rated-2', 'rated-2', 0);
INSERT INTO `piano_terms` VALUES (12, 'rated-3', 'rated-3', 0);
INSERT INTO `piano_terms` VALUES (13, 'rated-4', 'rated-4', 0);
INSERT INTO `piano_terms` VALUES (14, 'rated-5', 'rated-5', 0);
INSERT INTO `piano_terms` VALUES (15, 'Uncategorized', 'uncategorized', 0);
INSERT INTO `piano_terms` VALUES (16, 'Clothing', 'clothing', 0);
INSERT INTO `piano_terms` VALUES (17, 'Tshirts', 'tshirts', 0);
INSERT INTO `piano_terms` VALUES (18, 'Hoodies', 'hoodies', 0);
INSERT INTO `piano_terms` VALUES (19, 'Accessories', 'accessories', 0);
INSERT INTO `piano_terms` VALUES (20, 'Music', 'music', 0);
INSERT INTO `piano_terms` VALUES (21, 'Decor', 'decor', 0);
INSERT INTO `piano_terms` VALUES (22, 'Blue', 'blue', 0);
INSERT INTO `piano_terms` VALUES (23, 'Green', 'green', 0);
INSERT INTO `piano_terms` VALUES (24, 'Red', 'red', 0);
INSERT INTO `piano_terms` VALUES (25, 'Large', 'large', 0);
INSERT INTO `piano_terms` VALUES (26, 'Medium', 'medium', 0);
INSERT INTO `piano_terms` VALUES (27, 'Small', 'small', 0);
INSERT INTO `piano_terms` VALUES (28, 'Gray', 'gray', 0);
INSERT INTO `piano_terms` VALUES (29, 'Yellow', 'yellow', 0);
INSERT INTO `piano_terms` VALUES (30, 'heading menu', 'heading-menu', 0);
INSERT INTO `piano_terms` VALUES (31, 'Tin tức', 'tin-tuc', 0);
INSERT INTO `piano_terms` VALUES (32, 'Hệ thống cửa hàng', 'he-thong-cua-hang', 0);
INSERT INTO `piano_terms` VALUES (33, 'Main menu', 'main-menu', 0);
INSERT INTO `piano_terms` VALUES (34, 'Piano điện', 'digital-piano', 0);
INSERT INTO `piano_terms` VALUES (35, 'Upright Piano', 'upright-piano', 0);
INSERT INTO `piano_terms` VALUES (36, 'Grand Piano', 'grand-piano', 0);
INSERT INTO `piano_terms` VALUES (37, 'Stage Piano', 'state-piano', 0);
INSERT INTO `piano_terms` VALUES (38, 'organ', 'organ', 0);
INSERT INTO `piano_terms` VALUES (39, 'Cheap piano', 'cheap-piano', 0);
INSERT INTO `piano_terms` VALUES (40, 'Piano accessories', 'piano-accessories', 0);
INSERT INTO `piano_terms` VALUES (41, 'Homepage Category display', 'homepage-category-display', 0);
INSERT INTO `piano_terms` VALUES (42, 'a', 'a', 0);

-- ----------------------------
-- Table structure for piano_usermeta
-- ----------------------------
DROP TABLE IF EXISTS `piano_usermeta`;
CREATE TABLE `piano_usermeta`  (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`umeta_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_usermeta
-- ----------------------------
INSERT INTO `piano_usermeta` VALUES (1, 1, 'nickname', 'admin');
INSERT INTO `piano_usermeta` VALUES (2, 1, 'first_name', '');
INSERT INTO `piano_usermeta` VALUES (3, 1, 'last_name', '');
INSERT INTO `piano_usermeta` VALUES (4, 1, 'description', '');
INSERT INTO `piano_usermeta` VALUES (5, 1, 'rich_editing', 'true');
INSERT INTO `piano_usermeta` VALUES (6, 1, 'syntax_highlighting', 'true');
INSERT INTO `piano_usermeta` VALUES (7, 1, 'comment_shortcuts', 'false');
INSERT INTO `piano_usermeta` VALUES (8, 1, 'admin_color', 'fresh');
INSERT INTO `piano_usermeta` VALUES (9, 1, 'use_ssl', '0');
INSERT INTO `piano_usermeta` VALUES (10, 1, 'show_admin_bar_front', 'true');
INSERT INTO `piano_usermeta` VALUES (11, 1, 'locale', '');
INSERT INTO `piano_usermeta` VALUES (12, 1, 'piano_capabilities', 'a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `piano_usermeta` VALUES (13, 1, 'piano_user_level', '10');
INSERT INTO `piano_usermeta` VALUES (14, 1, 'dismissed_wp_pointers', '');
INSERT INTO `piano_usermeta` VALUES (15, 1, 'show_welcome_panel', '1');
INSERT INTO `piano_usermeta` VALUES (17, 1, 'piano_dashboard_quick_press_last_post_id', '66');
INSERT INTO `piano_usermeta` VALUES (18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}');
INSERT INTO `piano_usermeta` VALUES (19, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}');
INSERT INTO `piano_usermeta` VALUES (20, 1, 'piano_product_import_error_log', 'a:0:{}');
INSERT INTO `piano_usermeta` VALUES (21, 1, 'closedpostboxes_acf-field-group', 'a:0:{}');
INSERT INTO `piano_usermeta` VALUES (22, 1, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}');
INSERT INTO `piano_usermeta` VALUES (23, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}');
INSERT INTO `piano_usermeta` VALUES (24, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:21:\"add-post-type-product\";i:1;s:23:\"add-post-type-carousels\";i:2;s:15:\"add-product_tag\";}');
INSERT INTO `piano_usermeta` VALUES (25, 1, 'session_tokens', 'a:3:{s:64:\"8192d11f9d4085ac8a5c4c8f81ee4fb0ceaf12564d605bc5aed3340bcef5177f\";a:4:{s:10:\"expiration\";i:1527218609;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36\";s:5:\"login\";i:1527045809;}s:64:\"fb7990da913b5e947c3889cdf6a3583353c128e3000d34320a302f54b5bcc823\";a:4:{s:10:\"expiration\";i:1527300824;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36\";s:5:\"login\";i:1527128024;}s:64:\"57be8780bcf673a74957d91d92ad657b6010de287c81939913436ddc1066ea43\";a:4:{s:10:\"expiration\";i:1528345706;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36\";s:5:\"login\";i:1527136106;}}');
INSERT INTO `piano_usermeta` VALUES (26, 1, 'nav_menu_recently_edited', '41');
INSERT INTO `piano_usermeta` VALUES (27, 1, 'closedpostboxes_dashboard', 'a:2:{i:0;s:17:\"dashboard_primary\";i:1;s:21:\"dashboard_quick_press\";}');
INSERT INTO `piano_usermeta` VALUES (28, 1, 'metaboxhidden_dashboard', 'a:0:{}');
INSERT INTO `piano_usermeta` VALUES (29, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:104:\"dashboard_right_now,dashboard_activity,woocommerce_dashboard_recent_reviews,woocommerce_dashboard_status\";s:4:\"side\";s:17:\"dashboard_primary\";s:7:\"column3\";s:21:\"dashboard_quick_press\";s:7:\"column4\";s:0:\"\";}');
INSERT INTO `piano_usermeta` VALUES (30, 1, 'acf_user_settings', 'a:0:{}');
INSERT INTO `piano_usermeta` VALUES (31, 1, 'piano_user-settings', 'libraryContent=browse');
INSERT INTO `piano_usermeta` VALUES (32, 1, 'piano_user-settings-time', '1527145575');
INSERT INTO `piano_usermeta` VALUES (33, 1, 'wc_last_active', '1527120000');
INSERT INTO `piano_usermeta` VALUES (34, 1, 'dismissed_no_secure_connection_notice', '1');
INSERT INTO `piano_usermeta` VALUES (35, 1, 'dismissed_store_notice_setting_moved_notice', '1');
INSERT INTO `piano_usermeta` VALUES (36, 1, 'dismissed_update_notice', '1');
INSERT INTO `piano_usermeta` VALUES (37, 1, 'closedpostboxes_nav-menus', 'a:0:{}');

-- ----------------------------
-- Table structure for piano_users
-- ----------------------------
DROP TABLE IF EXISTS `piano_users`;
CREATE TABLE `piano_users`  (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime(0) NOT NULL,
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `user_login_key`(`user_login`) USING BTREE,
  INDEX `user_nicename`(`user_nicename`) USING BTREE,
  INDEX `user_email`(`user_email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_users
-- ----------------------------
INSERT INTO `piano_users` VALUES (1, 'admin', '$P$Bws1BssuME9O6iTr8YVU6HyZSH3Mu81', 'admin', 'hieuhaono@gmail.com', '', '2018-05-16 03:29:56', '1526889086:$P$BY2bU6yqhK55JhHGNwuUlMe6cDHtSs.', 0, 'admin');

-- ----------------------------
-- Table structure for piano_wc_download_log
-- ----------------------------
DROP TABLE IF EXISTS `piano_wc_download_log`;
CREATE TABLE `piano_wc_download_log`  (
  `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime(0) NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `user_ip_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`download_log_id`) USING BTREE,
  INDEX `permission_id`(`permission_id`) USING BTREE,
  INDEX `timestamp`(`timestamp`) USING BTREE,
  CONSTRAINT `piano_wc_download_log_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `piano_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_wc_webhooks
-- ----------------------------
DROP TABLE IF EXISTS `piano_wc_webhooks`;
CREATE TABLE `piano_wc_webhooks`  (
  `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime(0) NOT NULL,
  `date_created_gmt` datetime(0) NOT NULL,
  `date_modified` datetime(0) NOT NULL,
  `date_modified_gmt` datetime(0) NOT NULL,
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT 0,
  `pending_delivery` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`webhook_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_api_keys
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_api_keys`;
CREATE TABLE `piano_woocommerce_api_keys`  (
  `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `permissions` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `truncated_key` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`key_id`) USING BTREE,
  INDEX `consumer_key`(`consumer_key`) USING BTREE,
  INDEX `consumer_secret`(`consumer_secret`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_attribute_taxonomies
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_attribute_taxonomies`;
CREATE TABLE `piano_woocommerce_attribute_taxonomies`  (
  `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `attribute_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`attribute_id`) USING BTREE,
  INDEX `attribute_name`(`attribute_name`(20)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_woocommerce_attribute_taxonomies
-- ----------------------------
INSERT INTO `piano_woocommerce_attribute_taxonomies` VALUES (1, 'color', 'Color', 'select', 'menu_order', 0);
INSERT INTO `piano_woocommerce_attribute_taxonomies` VALUES (2, 'size', 'Size', 'select', 'menu_order', 0);

-- ----------------------------
-- Table structure for piano_woocommerce_downloadable_product_permissions
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_downloadable_product_permissions`;
CREATE TABLE `piano_woocommerce_downloadable_product_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `download_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `order_key` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `downloads_remaining` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `access_granted` datetime(0) NOT NULL,
  `access_expires` datetime(0) NULL DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`permission_id`) USING BTREE,
  INDEX `download_order_key_product`(`product_id`, `order_id`, `order_key`(16), `download_id`) USING BTREE,
  INDEX `download_order_product`(`download_id`, `order_id`, `product_id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_log
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_log`;
CREATE TABLE `piano_woocommerce_log`  (
  `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime(0) NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `level`(`level`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_order_itemmeta
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_order_itemmeta`;
CREATE TABLE `piano_woocommerce_order_itemmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `order_item_id`(`order_item_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(32)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_order_items
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_order_items`;
CREATE TABLE `piano_woocommerce_order_items`  (
  `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_item_id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_payment_tokenmeta
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_payment_tokenmeta`;
CREATE TABLE `piano_woocommerce_payment_tokenmeta`  (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`meta_id`) USING BTREE,
  INDEX `payment_token_id`(`payment_token_id`) USING BTREE,
  INDEX `meta_key`(`meta_key`(32)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_payment_tokens
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_payment_tokens`;
CREATE TABLE `piano_woocommerce_payment_tokens`  (
  `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`token_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_sessions
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_sessions`;
CREATE TABLE `piano_woocommerce_sessions`  (
  `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  UNIQUE INDEX `session_id`(`session_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_woocommerce_sessions
-- ----------------------------
INSERT INTO `piano_woocommerce_sessions` VALUES (9, '1', 'a:7:{s:8:\"customer\";s:707:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"VN\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"VN\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:19:\"hieuhaono@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";}', 1527218630);

-- ----------------------------
-- Table structure for piano_woocommerce_shipping_zone_locations
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_shipping_zone_locations`;
CREATE TABLE `piano_woocommerce_shipping_zone_locations`  (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`) USING BTREE,
  INDEX `location_id`(`location_id`) USING BTREE,
  INDEX `location_type_code`(`location_type`(10), `location_code`(20)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_woocommerce_shipping_zone_locations
-- ----------------------------
INSERT INTO `piano_woocommerce_shipping_zone_locations` VALUES (1, 1, 'VN', 'country');

-- ----------------------------
-- Table structure for piano_woocommerce_shipping_zone_methods
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_shipping_zone_methods`;
CREATE TABLE `piano_woocommerce_shipping_zone_methods`  (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `method_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`instance_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_woocommerce_shipping_zone_methods
-- ----------------------------
INSERT INTO `piano_woocommerce_shipping_zone_methods` VALUES (1, 1, 'flat_rate', 1, 1);
INSERT INTO `piano_woocommerce_shipping_zone_methods` VALUES (0, 2, 'flat_rate', 1, 1);

-- ----------------------------
-- Table structure for piano_woocommerce_shipping_zones
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_shipping_zones`;
CREATE TABLE `piano_woocommerce_shipping_zones`  (
  `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`zone_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piano_woocommerce_shipping_zones
-- ----------------------------
INSERT INTO `piano_woocommerce_shipping_zones` VALUES (1, 'Vietnam', 0);

-- ----------------------------
-- Table structure for piano_woocommerce_tax_rate_locations
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_tax_rate_locations`;
CREATE TABLE `piano_woocommerce_tax_rate_locations`  (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `location_code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`location_id`) USING BTREE,
  INDEX `tax_rate_id`(`tax_rate_id`) USING BTREE,
  INDEX `location_type_code`(`location_type`(10), `location_code`(20)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piano_woocommerce_tax_rates
-- ----------------------------
DROP TABLE IF EXISTS `piano_woocommerce_tax_rates`;
CREATE TABLE `piano_woocommerce_tax_rates`  (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT 0,
  `tax_rate_shipping` int(1) NOT NULL DEFAULT 1,
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`) USING BTREE,
  INDEX `tax_rate_country`(`tax_rate_country`) USING BTREE,
  INDEX `tax_rate_state`(`tax_rate_state`(2)) USING BTREE,
  INDEX `tax_rate_class`(`tax_rate_class`(10)) USING BTREE,
  INDEX `tax_rate_priority`(`tax_rate_priority`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
