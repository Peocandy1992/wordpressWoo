<?php

/**
 * Parent hook 	storefront_homepage
 * Functions hooked in to storefront_page add_action
 *
 * @hooked storefront_homepage_header      - 10
 * @hooked storefront_page_content         - 20
 */

function remove_storefront_hompage(){
    remove_action( 'storefront_homepage','storefront_homepage_header',10);
    remove_action( 'storefront_homepage','storefront_page_content',20);
}
add_action( 'storefront_homepage', 'remove_storefront_hompage' );