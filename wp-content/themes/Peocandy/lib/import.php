<?php
//define cons THEME_URL and THEME_URI


// add css
function theme_add_css() {
    wp_enqueue_style( "fontawesom-css", "https://use.fontawesome.com/releases/v5.0.13/css/all.css" );
    wp_enqueue_style( "carousel-css", THEME_URI."/js/owl-carousel/owl.carousel.css" );
    wp_enqueue_style( "theme-main-css", THEME_URI."/css/main.css" );

    //wp_enqueue_style( "bt-responsive", THEME_URI."/asset/css/responsive.css" );
}
add_action( "wp_enqueue_scripts", "theme_add_css" );

// add js
function theme_add_js() {
    wp_enqueue_script( "carousel-js", THEME_URI."/js/owl-carousel/owl.carousel.js", array( "jquery" ), "1.0", true );
    wp_enqueue_script( "theme-main-js", THEME_URI."/js/main.js", array( "jquery" ), "1.0", true );
    //wp_enqueue_script( "bt-responsive", THEME_URI."/asset/js/responsive.js", array( "jquery" ), "1.0", true );
}
add_action("wp_enqueue_scripts","theme_add_js");

//add font-family

function theme_add_font(){
//    wp_enqueue_style("theme_lato_font",THEME_URI."/fonts/lato-heavy.ttf", false );
    wp_enqueue_style( "theme_roboto_font", "https://fonts.googleapis.com/css?family=Roboto" );
    wp_enqueue_style( "theme_lato_font", "https://fonts.googleapis.com/css?family=Lato" );
}
add_action("wp_enqueue_scripts","theme_add_font");