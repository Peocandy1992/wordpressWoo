<?php
//Tạo menu trong admin
add_action('admin_menu', 'create_theme_options');

if(!function_exists('create_theme_options')){
    function create_theme_options() {
        //add_menu_page( string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '', string $icon_url = '', int $position = null )
        /*
        @page_title : Tiêu đề menu trên tab
        @menu_title : Tiêu đề hiện thị bên list menu
        @capability : Quyền hạn sử dụng : xem thêm : https://codex.wordpress.org/Roles_and_Capabilities
        @menu_slug : Đường dẫn thiết lập cho phần option này
        */
        acf_add_options_page(array(
            'page_title'  => 'Peocandy Theme options',
            'menu_title'  => 'Peocandy Theme Options',
            'menu_slug'   => 'Peocandy-theme',
            'capability'  => 'edit_posts',
            'redirect'    => true,
            'icon_url'    => 'dashicons-smiley',
            'position'    => 6
        ));
//        $page = add_menu_page( 'Peocandy Theme option', 'Peocandy Theme Options', 'manage_options', 'my-optionpage', 'my_settings_page', 'dashicons-smiley', 6 );
    }
}

