<?php
//homepage
/** Parent hook homepage
 * Functions hooked in to homepage action
 *
 * @hooked storefront_homepage_content - 10
 * @hooked storefront_product_categories - 20
 * @hooked storefront_recent_products - 30
 * @hooked storefront_featured_products - 40
 * @hooked storefront_popular_products - 50
 * @hooked storefront_on_sale_products - 60
 * @hooked storefront_best_selling_products - 70
 *
 *
 * Functions hooked in to storefront_page add_action
 *
 * @hooked storefront_homepage_header      - 10
 * @hooked storefront_page_content         - 20
 *
 * Add content before Homepage Product Category section title
 * @action storefront_homepage_before_product_categories
 *
 * Add content after Homepage Product Category section title
 * @action storefront_homepage_after_product_categories_title
 *
 * Add content after Homepage Featured Products section title
 * @action storefront_homepage_after_product_categories
 *
 *
 * Add content before Homepage Recent Products section title
 * @action storefront_homepage_before_recent_products
 *
 * Add content after Homepage Recent Products section title
 * @action storefront_homepage_after_recent_products_title
 *
 * Add content after Homepage Recent Products
 * @action 	storefront_homepage_after_recent_products
 *
 * Add content before Homepage Featured Products section title
 * @action 	storefront_homepage_before_featured_products
 *
 * Add content before Homepage Popular Products section title
 * @action 	storefront_homepage_after_featured_products_title
 *
 * Add content after Homepage Featured Products
 * @action storefront_homepage_after_featured_products
 *
 * @action storefront_homepage_before_popular_products
 */



//remove actions homepage content
function remove_storefront_homepage() {
//    remove_action( 'homepage','storefront_homepage_content',10);
//    remove_action( 'homepage','storefront_product_categories',20);
//    remove_action( 'homepage','storefront_recent_products',30);
//    remove_action( 'homepage','storefront_featured_products',40);
//    remove_action( 'homepage','storefront_popular_products',50);
//    remove_action( 'homepage','storefront_on_sale_products',60);
//    remove_action( 'homepage','storefront_best_selling_products',70);
}
add_action( 'homepage', 'remove_storefront_homepage' );
