<?php
/**
 * Header hook
 * @hooked storefront_before_site :	Add content after body opening
 * @hooked storefront_before_header : Add content before header tag
 *
 * Parent Hook : storefront_header : Displays content between header tags
 * Functions hooked into storefront_header action
 *
 * @hooked storefront_header_container                 - 0
 * @hooked storefront_skip_links                       - 5
 * @hooked storefront_social_icons                     - 10
 * @hooked storefront_site_branding                    - 20
 * @hooked storefront_secondary_navigation             - 30
 * @hooked storefront_product_search                   - 40
 * @hooked storefront_header_container_close           - 41
 * @hooked storefront_primary_navigation_wrapper       - 42
 * @hooked storefront_primary_navigation               - 50
 * @hooked storefront_header_cart                      - 60
 * @hooked storefront_primary_navigation_wrapper_close - 68
 *
 * Functions hooked into storefront_before_content action : Adds content at the top of site content div
 *
 * @hooked storefront_header_widget_region - 10
 * @hooked woocommerce_breadcrumb - 10
 *
 *
 */

function peocandy_theme_setup(){
    // PHP Script here

}
add_action( 'storefront_before_site', 'peocandy_theme_setup' );


