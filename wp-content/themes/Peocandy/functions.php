 <?php
 /**
  * Not be edited.
  *
  * @category Peocandy Theme
  * @package  Templates
  * @author   Ngo Hieu
  * @license  GPL-2.0+
  */

 define( 'THEME_URL', get_stylesheet_directory() );
 define( 'THEME_URI', get_stylesheet_directory_uri() );
 define( 'LIB', THEME_URL . '/lib' );
 define( 'NO_THUMB', '<img src="' .THEME_URI. '/images/no_thumb.png" />' );

 //require file handle
 require_once('lib/custom_options.php');
 require_once('lib/homepage.php');
 require_once('lib/header.php');
 require_once('lib/content-homepage.php');

 //require import library
 require_once('lib/import.php');

 //remove function
 function remove_sf_actions() {
//     custom heading
     remove_action( 'storefront_header', 'storefront_header_cart', 60 );
     remove_action( 'storefront_header', 'storefront_product_search', 40 );
     remove_action( 'storefront_header', 'storefront_site_branding', 20 );
     remove_action( 'storefront_header', 'storefront_skip_links', 5 );
     remove_action( 'storefront_header', 'storefront_secondary_navigation',30);
//     custom home_page
     remove_action( 'homepage', 'storefront_homepage_content',10);
     remove_action( 'homepage', 'storefront_product_categories',20);
 }
 add_action( 'init', 'remove_sf_actions' );

 function custom_storefront_cart_link() {
     ?>
     <div class="menu-cart clearfix">
     <a class="custom_cart_contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>">
         <span class="count"><?php echo wp_kses_data( sprintf( _n( '%d item', 'GIỎ HÀNG (%d)', WC()->cart->get_cart_contents_count(), 'storefront' ), WC()->cart->get_cart_contents_count() ) );?></span>
     </a>
     </div>
     <?php
 }
 add_action('storefront_header','custom_storefront_cart_link',61);


 function custom_storefront_search(){
     ?>
     <div class="site-search">
         <div class="widget woocommerce widget_product_search">
         <form role="search" method="get" class="woocommerce-product-search custom_woo_product_search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
             <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
             <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Tìm kiếm&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
        <!--     <button type="submit" value="--><?php //echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?><!--">--><?php //echo esc_html_x( 'Search', 'submit button', 'woocommerce' ); ?><!--</button>-->
             <input type="hidden" name="post_type" value="product" />
         </form>
         </div>
     </div>
     <?php
 }
 add_action('storefront_header','custom_storefront_search',40);

 function custom_storefront_site_branding(){
     ?>
     <div class="logo_branding clearfix">
         <a href="<?php echo get_bloginfo('url'); ?>" title="Piano Site">
             <img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/Peocandy/images/logo@2x.png" class="top_branding" alt="">
         </a>
     </div>
   <?php
 }
 add_action('storefront_header','custom_storefront_site_branding',20);

 function custom_storefront_heading(){
     ?>
     <div class="header-content">
         <div class="col-full">
             <div class="left_heading_info">
                 <div class="heading_info">
                     <span><i class="far fa-envelope"></i></span><?php echo get_field('menu_top_email','option'); ?>
                 </div>

                 <div class="heading_info">
                     <span><i class="fas fa-phone"></i></span><?php echo get_field('menu_top_phone','option'); ?>
                 </div>

                 <div class="heading_info">
                     <span><i class="fas fa-mobile-alt"></i></span><?php echo get_field('menu_top_hotline','option'); ?>
                 </div>
             </div>
             <div class="right_heading_info">
                 <?php wp_nav_menu(array('theme_location'=>'secondary')); ?>
             </div>
         </div>
     </div>
     <?php
 }
 add_action('storefront_before_header','custom_storefront_heading');

 function custom_storefront_homepage(){
     $service = get_field('service','option');
     $numCol = count($service);


     ?>
     <div id="service_list" class="clearfix">
         <?php
         foreach ($service as $key=>$val){

             ?>
            <div class="service_item col-<?php echo $numCol ?>">
                <a href="<?php echo ($val["service_link"]) ? $val["service_link"]: "#"; ?>">
                    <img class="icon_service" src="<?php echo ($val["service_icon"]) ? $val["service_icon"] : NO_THUMB ?>" alt="<?php echo ($val["service_name"]) ? $val["service_name"]: ""; ?>">
                    <div class="service_title">
                        <?php echo ($val["service_name"]) ? $val["service_name"]: "Input services name"; ?>
                    </div>
                </a>
            </div>
         <?php
         }
         ?>
     </div>

    <div id="home_slider" class="clearfix">
        <?php
            echo do_shortcode('[carousel_slide id="60"]');
        ?>
     </div>
 <?php
 }
 add_action('homepage','custom_storefront_homepage',10);

 //add custom menu display
 function wpb_custom_new_menu() {
     register_nav_menu('home_page_category',__( 'List category home page' ));
 }
 add_action( 'init', 'wpb_custom_new_menu' );

 function storefront_woocommerce_brands_homepage_section(){
     class category_home_walker extends Walker_Nav_Menu {

         /**
          * Phương thức start_lvl()
          * Được sử dụng để hiển thị các thẻ bắt đầu cấu trúc của một cấp độ mới trong menu. (ví dụ: <ul class="sub-menu">)
          * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
          * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
          * @param array $args | Các tham số trong hàm wp_nav_menu()
          **/
         public function start_lvl( &$output, $depth = 0, $args = array() )
         {
             $indent = str_repeat("\t", $depth);
             $output .= "<span class=\"sub-intro\">Menu con</span>";
             $output .= "\n$indent<ul class=\"sub-menu\">\n";
         }

         /**
          * Phương thức end_lvl()
          * Được sử dụng để hiển thị đoạn kết thúc của một cấp độ mới trong menu. (ví dụ: </ul> )
          * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
          * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
          * @param array $args | Các tham số trong hàm wp_nav_menu()
          **/
         public function end_lvl( &$output, $depth = 0, $args = array() )
         {
             $indent = str_repeat("\t", $depth);
             $output .= "$indent</ul>\n";
         }

         /**
          * Phương thức start_el()
          * Được sử dụng để hiển thị đoạn bắt đầu của một phần tử trong menu. (ví dụ: <li id="menu-item-5"> )
          * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
          * @param string $item | Dữ liệu của các phần tử trong menu
          * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
          * @param array $args | Các tham số trong hàm wp_nav_menu()
          * @param interger $id | ID của phần tử hiện tại
          **/
         public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
         {
             $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

                $classes = empty( $item->classes ) ? array() : (array) $item->classes;
                $classes[] = 'custom-menu-item-' . $item->ID;

                /**
                 * Filter the CSS class(es) applied to a menu item's list item element.
                 *
                 * @since 3.0.0
                 * @since 4.1.0 The `$depth` parameter was added.
                 *
                 * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
                 * @param object $item    The current menu item.
                 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
                 * @param int    $depth   Depth of menu item. Used for padding. */

                $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
                $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

                 /**
                 * Filter the ID applied to a menu item's list item element.
                 *
                 * @since 3.0.1
                 * @since 4.1.0 The `$depth` parameter was added.
                 *
                 * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
                 * @param object $item    The current menu item.
                 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
                 * @param int    $depth   Depth of menu item. Used for padding.
                 */
                $id = apply_filters( 'nav_menu_item_id', 'cus-menu-item-'. $item->ID, $item, $args, $depth );
                $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
                $output .= $indent . '<li' . $id . $class_names .'>';

                $atts = array();
                $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
                $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
                $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
                $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

                /**
                 * Filter the HTML attributes applied to a menu item's anchor element.
                 *
                 * @since 3.6.0
                 * @since 4.1.0 The `$depth` parameter was added.
                 *
                 * @param array $atts {
                 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
                 *
                 *     @type string $title  Title attribute.
                 *     @type string $target Target attribute.
                 *     @type string $rel    The rel attribute.
                 *     @type string $href   The href attribute.
                 * }
                 *     @param object $item  The current menu item.
                 *     @param array  $args  An array of {@see wp_nav_menu()} arguments.
                 *     @param int    $depth Depth of menu item. Used for padding.
                 */
                $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

                $attributes = '';
                foreach ( $atts as $attr => $value ) {
                    if ( ! empty( $value ) ) {
                        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                        $attributes .= ' ' . $attr . '="' . $value . '"';
                    }
                }

                $item_output = $args->before;

                $thumbnail_id = get_woocommerce_term_meta( $item->object_id, 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id );

                $item_output .= '<a'. $attributes .' class="category_name"><img src="'.$image.'" />';
                /** This filter is documented in wp-includes/post-template.php */
                $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
                $item_output .= '</a>';
                $item_output .= $args->after;

                /**
                 * Filter a menu item's starting output.
                 *
                 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
                 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
                 * no filter for modifying the opening and closing `<li>` for a menu item.
                 *
                 * @since 3.0.0
                 *
                 * @param string $item_output The menu item's starting HTML output.
                 * @param object $item        Menu item data object.
                 * @param int    $depth       Depth of menu item. Used for padding.
                 * @param array  $args        An array of {@see wp_nav_menu()} arguments.
                 */
                $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
         }

         /**
          * Phương thức end_el()
          * Được sử dụng để hiển thị đoạn kết thúc của một phần tử trong menu. (ví dụ: </li> )
          * @param string $output | Sử dụng để thêm nội dung vào những gì hiển thị ra bên ngoài
          * @param string $item | Dữ liệu của các phần tử trong menu
          * @param interger $depth | Cấp độ hiện tại của menu. Cấp độ 0 là lớn nhất.
          * @param array $args | Các tham số trong hàm wp_nav_menu()
          * @param interger $id | ID của phần tử hiện tại
          **/
         public function end_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
         {
             $output .= "</li>\n";
         }
     } // end class category_home_walker

     echo "<div class='category_home'>";
     echo "<h4 class='category_home_title'>DANH MỤC SẢN PHẨM</h4>";
         wp_nav_menu( array(
             'theme_location' => 'home_page_category',
             'container_class' => 'home_page_category',
             'walker' => new category_home_walker
         ) );

    echo "</div>";
 }add_action('homepage','storefront_woocommerce_brands_homepage_section',20);

