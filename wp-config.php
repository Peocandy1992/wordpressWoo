<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'piano_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&OC2PKEWPwcAC{JvkO50bx${`7*?duagYsSx]BRTi>?E 1XZHDul(7j-;m],s0O[');
define('SECURE_AUTH_KEY',  'Uez.|YpK8}/]nUDWvql=uvachp>!nIvwCpkRGQPJww$|`YK>&u?96G0.N,qz!RVm');
define('LOGGED_IN_KEY',    '4!PJ5jAfRgnV]{T%5VJ,im?GqBt&k`I/7~S7Q7K@-^gau NS)vPEI##|dgTE&$2g');
define('NONCE_KEY',        'g#0gzk/LXL{vK}joMC0 58_t1n{zF)2r^{)LgVFkmvrjH>u {>EMMVt#lAmwLXGZ');
define('AUTH_SALT',        '55Hxf|s~Dm8AYTh^e)J}2h@qPi$Wf*+IKYE7(4k]gOaa?-0%$KJ])*b4pSmE,7ef');
define('SECURE_AUTH_SALT', 'etI4QO`3J.3`SBzb<cS)E_<mvNM%4^Qw?ZYULWy:0.(Bh!<mK2+muA%PeT~d+!~z');
define('LOGGED_IN_SALT',   '0t|0cNBq)*B9RHrXSobxo@q;#}dYMfSB.7us3j^B&4W4mhumjJry:dCK7;p!Tg Y');
define('NONCE_SALT',       'L`hc@yg073Q[5Ojo=V7X1-[1s7N]x>;;spZ5g=xAq}~k#:O]3{gBp(q3uI}6`y,d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'piano_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
// define('WP_ALLOW_REPAIR', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
